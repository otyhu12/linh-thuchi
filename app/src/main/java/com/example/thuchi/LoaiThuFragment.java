package com.example.thuchi;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onGetLoai;
import com.example.thuchi.API.apiLoai.onPostLoai;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.MyAdapter.AdapterLoaiThu;
import com.example.thuchi.SQLite.loaiThuDAO;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoaiThuFragment extends Fragment implements onGetLoai, onPostLoai {

    //Dialog and floatactionbutton
    FloatingActionButton addLoaiThu;
    EditText et_AddLoaiThu;
    //SwipeRefreshLayout swipeRefreshLayout;
    //Dialog and floatActionButton

    //RecyclerView
    RecyclerView recyclerViewLoaiThu;
    ArrayList<modelLoai> listLoaiThu = new ArrayList<modelLoai>();
    //RecyclerView

    //search
    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;
    AdapterLoaiThu adapter;
    View view;
    //search

    loaiThuDAO loaiThuDAO;
    modelLoai loaiThu;
    public SharePre sharePre;
    public LoaiApi loaiApi;

    public LoaiThuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_loai_thu, null);

        addData();

        capnhatGiaodienLoaiThu();


        return view;
    }

    //have to import this void new
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    // have to import this void.


    // Create option menu for toolbar equal lookup, Just use for fragment
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //Have to add Clear with tabLayout
        menu.clear();
        //Have to add Clear with tablayou
        inflater.inflate(R.menu.menu_for_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        sharePre = new SharePre(getContext());
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    //important
                    adapter.getFilter().filter(newText);
                    //important
                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }
    //create option menu for toolbar equal lookup, Just use for fragment


    @SuppressLint("RestrictedApi")
    public void addData() {
        loaiApi = new LoaiApi();
        loaiApi.onGetLoai = this;
        loaiApi.onPostLoai = this;
        //Even action button view dialog
        addLoaiThu = view.findViewById(R.id.addLoaiThu);
        sharePre = new SharePre(getContext());
        getThu();
        if (sharePre.getCheckPemission()) {
            addLoaiThu.setVisibility(View.GONE);
        }
        addLoaiThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                LayoutInflater inf = LoaiThuFragment.this.getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_one, null);

                et_AddLoaiThu = view1.findViewById(R.id.et_AddLoaiThu);

                alertDialog.setView(view1);

                alertDialog.setNegativeButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tenLoaiThu = et_AddLoaiThu.getText().toString();

                        //SQLite
                        loaiThu = new modelLoai(tenLoaiThu, true);
                        Gson gson = new Gson();
                        //SQLite
                        capnhatGiaodienLoaiThu();
                        Map<String, Object> map = new HashMap<>();
                        map.put("object", gson.toJson(loaiThu));
                        loaiApi.AddData(sharePre.getIDUsername(), map);
                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });
                alertDialog.show();
                //dialog

            }
        });
        //Even action button view dialog
    }

    public void getThu() {
        loaiApi.getData(sharePre.getIDUsername(), true);
        MainActivity.progressDialogue.show();
    }

    public void capnhatGiaodienLoaiThu() {
        recyclerViewLoaiThu = view.findViewById(R.id.recyclerView);

        //View Loai Thu
        recyclerViewLoaiThu.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        adapter = new AdapterLoaiThu(listLoaiThu, getContext(), this);

        recyclerViewLoaiThu.setLayoutManager(linearLayoutManager);
        recyclerViewLoaiThu.setAdapter(adapter);
        MainActivity.progressDialogue.dismiss();
    }


    @Override
    public void onGetSuss(ArrayList<modelLoai> modelLoais) {
        listLoaiThu.clear();
        listLoaiThu = modelLoais;
        capnhatGiaodienLoaiThu();
    }

    @Override
    public void fallGet(String e) {

    }

    @Override
    public void onPostSuss(ArrayList<modelLoai> modelLoais) {
        getThu();
    }

    @Override
    public void fallPost(String e) {

    }
}
