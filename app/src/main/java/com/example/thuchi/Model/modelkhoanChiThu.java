package com.example.thuchi.Model;

public class modelkhoanChiThu {
    public String _id = "";
    public String tenKhoan = "";
    public String noiDung = "";
    public String ngay = "";
    public String idnguoinhap = "";
    public String nguoiNhap = "";
    public int _idLoai = 0;
    public String tenLoai = "";

    public modelkhoanChiThu() {
    }

    public modelkhoanChiThu(String tenKhoan, String noiDung, String ngay, String idnguoinhap, String nguoiNhap, int _idLoai, String tenLoai) {
        this.tenKhoan = tenKhoan;
        this.noiDung = noiDung;
        this.ngay = ngay;
        this.idnguoinhap = idnguoinhap;
        this.nguoiNhap = nguoiNhap;
        this._idLoai = _idLoai;
        this.tenLoai = tenLoai;
    }

    public modelkhoanChiThu(String _id, String tenKhoan, String noiDung, String ngay, String idnguoinhap, String nguoiNhap, int _idLoai, String tenLoai) {
        this._id = _id;
        this.tenKhoan = tenKhoan;
        this.noiDung = noiDung;
        this.ngay = ngay;
        this.idnguoinhap = idnguoinhap;
        this.nguoiNhap = nguoiNhap;
        this._idLoai = _idLoai;
        this.tenLoai = tenLoai;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTenKhoan() {
        return tenKhoan;
    }

    public void setTenKhoan(String tenKhoan) {
        this.tenKhoan = tenKhoan;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getNgay() {
        return ngay;
    }

    public void setNgay(String ngay) {
        this.ngay = ngay;
    }

    public String getIdnguoinhap() {
        return idnguoinhap;
    }

    public void setIdnguoinhap(String idnguoinhap) {
        this.idnguoinhap = idnguoinhap;
    }

    public String getNguoiNhap() {
        return nguoiNhap;
    }

    public void setNguoiNhap(String nguoiNhap) {
        this.nguoiNhap = nguoiNhap;
    }

    public int get_idLoai() {
        return _idLoai;
    }

    public void set_idLoai(int _idLoai) {
        this._idLoai = _idLoai;
    }
}

