package com.example.thuchi.Model;

public class modelLoai {
    public String id_firebase;
    public int _id;
    public String tenLoai;
    public boolean loai;

    public modelLoai() {
    }

    public modelLoai(String tenLoai) {
        this.tenLoai = tenLoai;
    }

    public modelLoai(String tenLoai, boolean loai) {
        this.tenLoai = tenLoai;
        this.loai = loai;
    }

    public modelLoai(int _id, String tenLoai) {
        this._id = _id;
        this.tenLoai = tenLoai;
    }

    public modelLoai(int _id, String tenLoai, boolean loai) {
        this._id = _id;
        this.tenLoai = tenLoai;
        this.loai = loai;
    }

    public modelLoai(String id_firebase, int _id, String tenLoai, boolean loai) {
        this.id_firebase = id_firebase;
        this._id = _id;
        this.tenLoai = tenLoai;
        this.loai = loai;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTenLoai() {
        return tenLoai;
    }

    public void setTenLoai(String tenLoai) {
        this.tenLoai = tenLoai;
    }

    public boolean isLoai() {
        return loai;
    }

    public void setLoai(boolean loai) {
        this.loai = loai;
    }
}
