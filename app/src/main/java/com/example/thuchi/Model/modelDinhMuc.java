package com.example.thuchi.Model;

public class modelDinhMuc {
    public String id_firebase = "";
    public String ten = "";
    public int number = 0;
    public int number_c = 0;
    public int loai = 0;
    public String id_NguoiTao = "";

    public modelDinhMuc() {
    }

    public int getNumber_c() {
        return number_c;
    }

    public void setNumber_c(int number_c) {
        this.number_c = number_c;
    }

    public String getId_NguoiTao() {
        return id_NguoiTao;
    }

    public void setId_NguoiTao(String id_NguoiTao) {
        this.id_NguoiTao = id_NguoiTao;
    }

    public modelDinhMuc(String id_firebase, String ten, int number, int number_c, int loai, String id_NguoiTao) {
        this.id_firebase = id_firebase;
        this.ten = ten;
        this.number = number;
        this.number_c = number_c;
        this.loai = loai;
        this.id_NguoiTao = id_NguoiTao;
    }

    public modelDinhMuc(String ten, int number, int number_c, int loai, String id_NguoiTao) {
        this.ten = ten;
        this.number = number;
        this.number_c = number_c;
        this.loai = loai;
        this.id_NguoiTao = id_NguoiTao;
    }

    public String getId_firebase() {
        return id_firebase;
    }

    public void setId_firebase(String id_firebase) {
        this.id_firebase = id_firebase;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getLoai() {
        return loai;
    }

    public void setLoai(int loai) {
        this.loai = loai;
    }
}
