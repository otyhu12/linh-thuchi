package com.example.thuchi.Model;

public class User {
    public String id = "";
    public String name = "";
    public String image = "";

    public User(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
