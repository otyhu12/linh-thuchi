package com.example.thuchi;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onGetLoai;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.apiThuChi.PostAPI;
import com.example.thuchi.API.apiThuChi.getAPI;
import com.example.thuchi.API.apiThuChi.onRepornDataget;
import com.example.thuchi.API.apiThuChi.onRepornDatapost;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.Model.modelkhoanChiThu;
import com.example.thuchi.MyAdapter.Adapter_KhoanThu;
import com.example.thuchi.MyAdapter.Adapter_SpinnerLoaiThu;
import com.example.thuchi.SQLite.khoanThuDAO;
import com.example.thuchi.SQLite.loaiThuDAO;
import com.example.thuchi.Ulits.ulits;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;


/**
 * A simple {@link Fragment} subclass.
 */
public class KhoanThuFragment extends Fragment implements onRepornDatapost, onRepornDataget, onGetLoai {
    public FloatingActionButton addKhoanThu;
    public EditText et_KhoanThu, et_NoidungKhoanThu;
    CalendarView calendarView;
    String date;
    SwipeRefreshLayout swipeRefreshLayout;
    //recycler
    RecyclerView recyclerViewKhoanThu;
    public ArrayList<modelkhoanChiThu> listKhoanThu = new ArrayList<modelkhoanChiThu>();
    //spinner
    public AlertDialog.Builder alertDialog;
    public Spinner spLoaiThu;
    loaiThuDAO loaiThuDAO;
    ArrayList<modelLoai> listLoaiThu = new ArrayList<modelLoai>();
    //search
    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;
    View view;
    Adapter_KhoanThu adapter_khoanThu;
    //search
    khoanThuDAO khoanThuDAO;
    modelkhoanChiThu khoanThu;
    public SharePre sharePre;
    public getAPI getAPI;
    public PostAPI postAPI;
    public LoaiApi loaiApi;
    public TextView date_present, date_now;

    public KhoanThuFragment() {
    }

    //have to import this void new
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //Have to add Clear with tabLayout
        menu.clear();
        //Have to add Clear with tablayou
        inflater.inflate(R.menu.menu_for_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    //important
                    adapter_khoanThu.getFilter().filter(newText);
                    //important

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }
    //create option menu for toolbar equal lookup, Just use for fragment


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_khoan_thu, null);
        addKhoanThu = view.findViewById(R.id.addKhoanThu);
        date_present = view.findViewById(R.id.date_present);
        date_now = view.findViewById(R.id.date_now);
        sharePre = new SharePre(getContext());
        getAPI = new getAPI(this);
        postAPI = new PostAPI(this);
        loaiApi = new LoaiApi();
        loaiApi.onGetLoai = this;
        setEventClick();
        getKhoanThu();
        init();
        //refreshLayout use support v4
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_thu);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        getKhoanThu();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 0000);
            }
        });
        //refreshLayout
        recyclerViewKhoanThu = view.findViewById(R.id.recyclerViewKhoanThu);

        //even of dialog
        addKhoanThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                init();
                date = com.example.thuchi.Date.date.getDayMax() + "/" + (com.example.thuchi.Date.date.getMonth()) + "/" + com.example.thuchi.Date.date.getYear();
                calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                        date = dayOfMonth + "/" + (month + 1) + "/" + year;
                    }
                });

                alertDialog.setNegativeButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tenKhoanThu = et_KhoanThu.getText().toString();
                        String noiDung = et_NoidungKhoanThu.getText().toString();
                        //date
                        String getDate = date;
                        //spinner
                        int _idLoaiThu = 0;
                        modelLoai loaiThu = new modelLoai();
                        try {
                            int index = spLoaiThu.getSelectedItemPosition();
                            loaiThu = listLoaiThu.get(index);
                            _idLoaiThu = loaiThu._id;

                        } catch (Exception e) {

                        }
                        khoanThu = new modelkhoanChiThu(tenKhoanThu, noiDung, getDate, apiManager.getUser().getId(), apiManager.getUser().getName(), _idLoaiThu, loaiThu.tenLoai);
                        Map<String, Object> map = new HashMap<>();
                        map.put("_id", khoanThu._id);
                        map.put("tenKhoan", khoanThu.tenKhoan);
                        map.put("noiDung", khoanThu.noiDung);
                        map.put("ngay", khoanThu.ngay);
                        map.put("idnguoinhap", khoanThu.idnguoinhap);
                        map.put("nguoiNhap", khoanThu.nguoiNhap);
                        map.put("_idLoai", khoanThu._idLoai);
                        map.put("tenLoai", khoanThu.tenLoai);

                        postAPI.AddData(getContext(), sharePre.getIDUsername() + "-thu", map);

                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });

                alertDialog.show();
                //Dialog
            }
        });
        //even of dialog

        return view;
    }
    public void setEventClick() {
        date_present.setText("01/" + com.example.thuchi.Date.date.getMonth() + "/" + com.example.thuchi.Date.date.getYear());
        date_now.setText(com.example.thuchi.Date.date.getDayMax() + "/" + com.example.thuchi.Date.date.getMonth() + "/" + com.example.thuchi.Date.date.getYear());


        date_present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_present, callback);

            }
        });

        date_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_now, callback1);

            }
        });

    }

    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_present.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            getKhoanThu();

        }
    };
    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback1 = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_now.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            getKhoanThu();
        }
    };
    public void init() {
        //Dialog
        alertDialog = new AlertDialog.Builder(view.getContext());
        LayoutInflater inf = KhoanThuFragment.this.getLayoutInflater();
        View view1 = inf.inflate(R.layout.dialog_khoanthu, null);

        et_KhoanThu = view1.findViewById(R.id.et_KhoanThu);
        et_NoidungKhoanThu = view1.findViewById(R.id.et_NoidungKhoanThu);

        //Xu ly spinner
        spLoaiThu = view1.findViewById(R.id.sp_LoaiThu);
        calendarView = view1.findViewById(R.id.calendarView);
        calendarView.setSelected(true);
        //date
        alertDialog.setView(view1);
    }
    public void CapNhatGiaoDienKhoanThu() {
        adapter_khoanThu = new Adapter_KhoanThu(getContext(), listKhoanThu, listLoaiThu, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewKhoanThu.setLayoutManager(linearLayoutManager);
        recyclerViewKhoanThu.setAdapter(adapter_khoanThu);


    }

    //lấy về khoản thu
    public void getKhoanThu() {
        getAPI.getData(sharePre.getIDUsername() + "-thu");
        loaiApi.getData(sharePre.getIDUsername(), true);
        MainActivity.progressDialogue.show();//hiển thị tiến trình progessload
    }

    public void CapNhatGiaoDienSpinnerKhoanThu() {
//        listLoaiThu = loaiThuDAO.ViewLoaiThu();
        Adapter_SpinnerLoaiThu spinnerLoaiThu = new Adapter_SpinnerLoaiThu(getContext(), listLoaiThu);
        spLoaiThu.setAdapter(spinnerLoaiThu);
    }


    //CapNhatGiaoDienChoFragmentKhi Chuyen TabLayout
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    //dữ liêu trả về khi lấy ra các khoản thu thành công
    @Override
    public void onGetDataSuss(ArrayList<modelkhoanChiThu> khoanChis) {

        listKhoanThu.clear();
        for (modelkhoanChiThu modelkhoanChiThu: khoanChis)
        {
            if (ulits.checkDateTo(date_present.getText().toString(), date_now.getText().toString(), modelkhoanChiThu.getNgay())) {

                listKhoanThu.add(modelkhoanChiThu);
            }
        }
        CapNhatGiaoDienKhoanThu();

    }

    //nếu thêm các khoản chi thành công


    @Override
    public void onpostDataSuss(JSONObject object) {
        getKhoanThu();
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onfall(String s) {
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onGetSuss(ArrayList<modelLoai> modelLoais) {
        listLoaiThu.clear();
        listLoaiThu = modelLoais;
        CapNhatGiaoDienSpinnerKhoanThu();
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void fallGet(String e) {

    }
}
