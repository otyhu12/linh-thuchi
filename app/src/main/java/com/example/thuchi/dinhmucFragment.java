package com.example.thuchi;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiDinhMuc.apiDinhMucManager;
import com.example.thuchi.API.apiDinhMuc.onDeleteDinhMuc;
import com.example.thuchi.API.apiDinhMuc.onGetDinhMuc;
import com.example.thuchi.API.apiDinhMuc.onPostDinhMuc;
import com.example.thuchi.API.apiDinhMuc.onUpdateDinhMuc;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.apiThuChi.getSumThuChi;
import com.example.thuchi.API.apiThuChi.onSumReporn;
import com.example.thuchi.Model.modelDinhMuc;
import com.example.thuchi.MyAdapter.Adapter_DinhMuc;
import com.example.thuchi.MyAdapter.Adapter_spinnerLoai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class dinhmucFragment extends Fragment implements onGetDinhMuc, onDeleteDinhMuc, onPostDinhMuc, onUpdateDinhMuc, onSumReporn {

    public EditText et_tenDinhMuc, edt_sotien;
    public Spinner spinner;
    public apiDinhMucManager apiDinhMucManager;
    public TextView ten,loai,tien;
    public SeekBar seekBar;

    public ArrayList<String> arrloaiDinhmuc;
    public SharePre sharePre;

    public ArrayList<modelDinhMuc> modelDinhMuc =  null;
    RecyclerView recyclerViewDinhMuc;
    public View view;
    public Adapter_DinhMuc adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    public com.example.thuchi.API.apiThuChi.getSumThuChi getSumThuChi;
    public dinhmucFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
     view = inflater.inflate(R.layout.frapment_dinhmuc, null);
        recyclerViewDinhMuc = view.findViewById(R.id.recyclerViewDinhMuc);
        apiDinhMucManager = new apiDinhMucManager();
        getSumThuChi = new getSumThuChi(this);
        apiDinhMucManager.onDeleteDinhMuc = this;
        apiDinhMucManager.onGetDinhMuc = this;
        apiDinhMucManager.onPostDinhMuc = this;
        apiDinhMucManager.onUpdateDinhMuc = this;
        sharePre = new SharePre(getContext());
        arrloaiDinhmuc = new ArrayList<String>();
        arrloaiDinhmuc.add("Ngày");
        arrloaiDinhmuc.add("Tuần");
        arrloaiDinhmuc.add("Tháng");
        arrloaiDinhmuc.add("Năm");

        getDinhmuc();
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        apiDinhMucManager.getData(sharePre.getIDUsername()+"-dinhmuc");
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 000);
            }
        });

        FloatingActionButton actionButton = view.findViewById(R.id.addDinhMuc);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
                LayoutInflater inf = dinhmucFragment.this.getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_dinhmuc, null);

                et_tenDinhMuc = view1.findViewById(R.id.et_tenDinhMuc);
                edt_sotien = view1.findViewById(R.id.et_sotien);
                spinner = view1.findViewById(R.id.sp_loai);
                capnhat();
                alertDialog.setView(view1);

                final int[] id_loai = {0};

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        id_loai[0] = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                alertDialog.setNegativeButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                            modelDinhMuc modelDinhMuc1 = new modelDinhMuc(et_tenDinhMuc.getText().toString(),Integer.parseInt(edt_sotien.getText().toString()), 0,id_loai[0], apiManager.getUser().id);
                            Map<String, Object> map = new HashMap<>();
                            map.put("id_firebase",modelDinhMuc1.id_firebase);
                            map.put("ten",modelDinhMuc1.ten);
                            map.put("loai",modelDinhMuc1.loai);
                            map.put("number",modelDinhMuc1.number);
                            map.put("number_c",modelDinhMuc1.number_c);
                            map.put("id_NguoiTao",modelDinhMuc1.id_NguoiTao);
                            apiDinhMucManager.AddData(sharePre.getIDUsername()+"-dinhmuc", map);
                        MainActivity.progressDialogue.show();
                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });

                alertDialog.show();
            }
        });
        return view;
    }

    public void getDinhmuc()
    {
        apiDinhMucManager.getData(sharePre.getIDUsername()+"-dinhmuc");
        MainActivity.progressDialogue.show();
    }
    public void capnhat()
    {
        Adapter_spinnerLoai spinnerLoaiChi = new Adapter_spinnerLoai(getContext(), arrloaiDinhmuc);
        spinner.setAdapter(spinnerLoaiChi);
    }
    public void capnhatGiaodien() {
        recyclerViewDinhMuc.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        adapter = new Adapter_DinhMuc(modelDinhMuc, arrloaiDinhmuc, getContext(), this);
        recyclerViewDinhMuc.setLayoutManager(linearLayoutManager);
        recyclerViewDinhMuc.setAdapter(adapter);
        MainActivity.progressDialogue.dismiss();
    }
    @Override
    public void onDeleteDinhMuc(String data) {

    }


    @Override
    public void onPostDinhMuc(String data) {
        apiDinhMucManager.getData(sharePre.getIDUsername()+"-dinhmuc");

    }

    @Override
    public void onUpdateDinhMuc(String data) {
        apiDinhMucManager.getData(sharePre.getIDUsername()+"-dinhmuc");
    }

    @Override
    public void onGetDinhMuc(ArrayList<modelDinhMuc> data) {
        if (data != null) {
            modelDinhMuc = data;
        }
        getSumThuChi.getData(sharePre.getIDUsername()+"-chi");
    }

    @Override
    public void fallDinhMuc(String e) {

    }

    public String getLoai(int i)
    {
        switch (i){
            case 0:
                return "Ngày";
            case 1:
                return "Tuần";
            case 2:
                return "Tháng";
            case 3:
                return "Năm";
            case 4:
                return "Ngày";
        }
        return "";
    }

    @Override
    public void SumSuss(int value) {
        ArrayList<modelDinhMuc> dinhMucs = new ArrayList<>();
        for (modelDinhMuc modelDinhMuc: modelDinhMuc)
        {
            modelDinhMuc.number_c = value;
            dinhMucs.add(modelDinhMuc);
        }
        modelDinhMuc = dinhMucs;
        capnhatGiaodien();
    }

    @Override
    public void fall(String e) {

    }
}