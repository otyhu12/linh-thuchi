package com.example.thuchi;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.facebookAPI;
import com.example.thuchi.API.apiThuChi.onRepornDataUser;
import com.example.thuchi.Model.User;
import com.facebook.AccessToken;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class BeginActivity extends AppCompatActivity implements onRepornDataUser {
    private static int SPLASH_TIME_OUT=2000;
    public facebookAPI facebookAPI;
    public SharePre sharePre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin);
        facebookAPI = new facebookAPI(this);
        sharePre = new SharePre(this);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.thuchi",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        final boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isLoggedIn)
                {
                    facebookAPI.RequestDataFacebook();

                }else {
                    Intent i = new Intent(BeginActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        },SPLASH_TIME_OUT);
    }

    @Override
    public void onGetDataSuss(User user) {
        apiManager.user = user;
        if ( sharePre.getIDUsername() == "") {
            sharePre.putIDUsename(user.id);
        }
        Intent i = new Intent(BeginActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onfall(String s) {

    }
}
