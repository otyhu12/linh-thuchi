package com.example.thuchi;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onGetLoai;
import com.example.thuchi.API.apiThuChi.getAPI;
import com.example.thuchi.API.apiThuChi.onRepornDataget;
import com.example.thuchi.Date.date;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.Model.modelkhoanChiThu;
import com.example.thuchi.Ulits.formatUlits;
import com.example.thuchi.Ulits.ulits;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class ThongKeFragmentBonFragment extends Fragment implements onGetLoai, onRepornDataget {

    PieChart  pieChart3;
    TextView tv_TongLoaiChi, date_present, date_now;
    ArrayList<modelkhoanChiThu> listKhoanChi;
    ArrayList<modelLoai> listLoaiChi;
    public LoaiApi loaiApi;
    public com.example.thuchi.API.apiThuChi.getAPI getAPI;
    public SharePre sharePre;
    public BarChart barChart;


    public ThongKeFragmentBonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    View view=inflater.inflate(R.layout.fragment_thong_ke_fragment_bon,null);
        pieChart3 = view.findViewById(R.id.piechart3);
        tv_TongLoaiChi = view.findViewById(R.id.tv_tongTungLoaiChi);
        date_present = view.findViewById(R.id.date_present);
        date_now = view.findViewById(R.id.date_now);
        listKhoanChi = new ArrayList<modelkhoanChiThu>();
        listLoaiChi = new ArrayList<modelLoai>();
        barChart =view.findViewById(R.id.barchart);
        sharePre = new SharePre(getContext());
        loaiApi = new LoaiApi();
        loaiApi.onGetLoai = this;
        getAPI = new getAPI(this);
        loaiApi.getData(sharePre.getIDUsername(),true);
        MainActivity.progressDialogue.show();
        setEventClick();
        return  view;
    }
    public void setEventClick()
    {
        date_present.setText("01/"+ date.getMonth()+"/"+date.getYear());
        date_now.setText(date.getDayMax()+"/"+ date.getMonth()+"/"+date.getYear());


        date_present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_present, callback);

            }
        });

        date_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_now, callback1);

            }
        });

    }
    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_present.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            init();
        }
    };
    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback1 = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_now.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            init();
        }
    };
    public void init()
    {
        long tongChi = 0;

        try {
            for (modelkhoanChiThu khoanChi : listKhoanChi) {
                long epdulieukc = Long.parseLong(khoanChi.noiDung);
                tongChi += epdulieukc;
            }

        } catch (Exception e) {
        }


        String TongC = "";
        //ThietLapHienThiPhanTram
        pieChart3.setUsePercentValues(true);
        //ThietLapHienThiPhanTram

        //Xu ly tinh tong tung loai thu
        long mangGan1[] = new long[listLoaiChi.size()];
        try {
            for (int i = 0; i < listLoaiChi.size(); i++) {
                for (int j = 0; j < listKhoanChi.size(); j++) {
                   if (ulits.checkDateTo(date_present.getText().toString(),date_now.getText().toString(),listKhoanChi.get(j).getNgay())) {
                        if (listLoaiChi.get(i).tenLoai.contains(listKhoanChi.get(j).tenLoai)) {
                            long ganCho = Long.parseLong(listKhoanChi.get(j).noiDung);
                            mangGan1[i] += ganCho;
                        }
                    }
                }
                TongC += "- " + listLoaiChi.get(i).tenLoai + ": " + formatUlits.formatVND(String.valueOf(mangGan1[i])) + "\n";
                tv_TongLoaiChi.setText(TongC);
            }

        } catch (Exception e) {

        }

        ///List one
        ArrayList<Entry> yvaluesC = new ArrayList<Entry>();

        try {

            for (int k = 0; k < listLoaiChi.size(); k++) {
                yvaluesC.add(new Entry((mangGan1[k] * 100) / tongChi, k));
            }
        } catch (Exception e) {

        }

        PieDataSet dataSet2 = new PieDataSet(yvaluesC, "");
        ///list One

        ///list two
        ArrayList<String> xVals2 = new ArrayList<String>();
        try {
            for (int k = 0; k < listLoaiChi.size(); k++) {
                xVals2.add(listLoaiChi.get(k).tenLoai);
            }
        } catch (Exception e) {

        }

        PieData data2 = new PieData(xVals2, dataSet2);
        ///list two

        data2.setValueFormatter(new PercentFormatter());

        pieChart3.setData(data2);
        //Set color
        dataSet2.setColors(ColorTemplate.VORDIPLOM_COLORS);
        //dataSet1.setColors(ColorTemplate.JOYFUL_COLORS);
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        //dataSet.setColors(ColorTemplate.LIBERTY_COLORS);
        //dataSet.setColors(ColorTemplate.PASTEL_COLORS);
        //set color
        pieChart3.setDescription("Biểu đồ tròn tổng từng Người chi");

        //text size and text color
        data2.setValueTextSize(13f);
        data2.setValueTextColor(Color.DKGRAY);
        //text size and text color

        //animation
        pieChart3.animateXY(1500, 1500);
        //animation
        //////////////////////////////////////////////////////////////////////////////////////


        ArrayList<String> labels = new ArrayList<String>();
        try {
            for (int k = 0; k < listLoaiChi.size(); k++) {
                labels.add(listLoaiChi.get(k).tenLoai);
            }
        } catch (Exception e) {

        }

        // create BarEntry for Bar Group 1
        ArrayList<BarEntry> bargroup1 = new ArrayList<>();
        try {

            for (int k = 0; k < listLoaiChi.size(); k++) {
                bargroup1.add(new BarEntry((mangGan1[k] * 100) / tongChi, k));
            }
        } catch (Exception e) {

        }

        BarDataSet bardataset = new BarDataSet(bargroup1, "Loại chi");

        BarData data = new BarData(labels, bardataset);
        barChart.setData(data); // set the data and list of labels into chart
        barChart.setDescription("Tỉ lệ % từng loại chi");  // set the description
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        barChart.animateY(5000);

    }
    @Override
    public void onGetSuss(ArrayList<modelLoai> modelLoais) {
        listLoaiChi.clear();
        listLoaiChi = modelLoais;
        getAPI.getData(sharePre.getIDUsername()+"-thu");
    }

    @Override
    public void fallGet(String e) {
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onGetDataSuss(ArrayList<modelkhoanChiThu> khoanChis) {
        listKhoanChi.clear();
        listKhoanChi = khoanChis;
        MainActivity.progressDialogue.dismiss();
        init();
    }

    @Override
    public void onfall(String s) {
        MainActivity.progressDialogue.dismiss();
    }
}
