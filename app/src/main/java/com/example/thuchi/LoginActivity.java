package com.example.thuchi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.facebookAPI;
import com.example.thuchi.API.apiThuChi.onRepornDataUser;
import com.example.thuchi.Model.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity implements onRepornDataUser {
    EditText et_Email, et_Password;
    Button bt_Login;
    private LoginButton mBtnLoginFacebook;
    private CallbackManager mCallbackManager;
    private SharePre sharePre;
    public facebookAPI facebookAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_Email = findViewById(R.id.et_Email);
        et_Password = findViewById(R.id.et_Password);
        bt_Login = findViewById(R.id.bt_login);
        sharePre = new SharePre(this);
        facebookAPI = new facebookAPI(this);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if (isLoggedIn) {
            IntentManager();
            facebookAPI.RequestDataFacebook();
        }

        bt_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email = et_Email.getText().toString();
                String Pass = et_Password.getText().toString();

                boolean kt = false;
                if (Email.length() == 0) {
                    kt = true;
                    et_Email.setError("Số điện thoại không được để trống");
                }
                if (Pass.length() < 6) {
                    kt = true;
                    et_Password.setError("Mã phải lớn hơn 6 kí tự");
                }
                if (kt == false) {
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                }

                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
            }
        });
        mCallbackManager = CallbackManager.Factory.create();

        mBtnLoginFacebook = (LoginButton) findViewById(R.id.bt_login_facebook);
        mBtnLoginFacebook.setReadPermissions("user_friends");
        mBtnLoginFacebook.setReadPermissions("public_profile");
        mBtnLoginFacebook.setReadPermissions("email");
        mBtnLoginFacebook.setReadPermissions("user_birthday");
        mBtnLoginFacebook.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                IntentManager();
                facebookAPI.RequestDataFacebook();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {
                Log.e("facebook1", "User ID: " + e);
                Toasty.success(getBaseContext(), e + "", Toast.LENGTH_LONG);
            }
        });

    }

    public void IntentManager() {
        Log.i("RESAULTS", AccessToken.getCurrentAccessToken() + "]");
        finish();
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onGetDataSuss(User user) {
        apiManager.user = user;//lưu trữ user vào biến tạm
        if (sharePre.getIDUsername() == "") {
            sharePre.putIDUsename(user.id);
        }
        IntentManager();
    }

    @Override
    public void onfall(String s) {

    }
}
