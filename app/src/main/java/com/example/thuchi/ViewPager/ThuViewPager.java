package com.example.thuchi.ViewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.thuchi.KhoanThuFragment;
import com.example.thuchi.LoaiThuFragment;

public class ThuViewPager extends FragmentStatePagerAdapter {


    public ThuViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new KhoanThuFragment();
                break;
            case 1:
                fragment = new LoaiThuFragment();

                break;
            default:
                return null;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
