package com.example.thuchi.ViewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.thuchi.KhoanChiFragment;
import com.example.thuchi.LoaiChiFragment;

public class ChiViewPager extends FragmentStatePagerAdapter {

    public ChiViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment1;
        switch (position) {
            case 0:
                fragment1 = new KhoanChiFragment();
                break;

            case 1:
                fragment1 = new LoaiChiFragment();

                break;
            default:
                return null;

        }

        return fragment1;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
