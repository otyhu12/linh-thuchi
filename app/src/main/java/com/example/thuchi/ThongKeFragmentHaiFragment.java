package com.example.thuchi;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onGetLoai;
import com.example.thuchi.API.apiThuChi.getAPI;
import com.example.thuchi.API.apiThuChi.onRepornDataget;
import com.example.thuchi.Date.date;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.Model.modelkhoanChiThu;
import com.example.thuchi.SQLite.khoanThuDAO;
import com.example.thuchi.Ulits.formatUlits;
import com.example.thuchi.Ulits.ulits;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThongKeFragmentHaiFragment extends Fragment implements onGetLoai, onRepornDataget {

    PieChart pieChart2;
    TextView tv_TongThu, tv_tongLoaiThu, date_present, date_now;
    ArrayList<modelkhoanChiThu> listKhoanThu;
    khoanThuDAO khoanThuDAO;
    ArrayList<modelLoai> listLoaiThu;
    public BarChart barChart;
    public LoaiApi loaiApi;
    public getAPI getAPI;
    public SharePre sharePre;

    public ThongKeFragmentHaiFragment() {
        // Required empty public
    }

    @Override
    public void onResume() {
        super.onResume();
        loaiApi.getData(sharePre.getIDUsername(), true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_thong_ke_fragment_hai, null);
        sharePre = new SharePre(getContext());
        loaiApi = new LoaiApi();
        loaiApi.onGetLoai = this;
        getAPI = new getAPI(this);
        listLoaiThu = new ArrayList<>();
        listKhoanThu = new ArrayList<>();
        pieChart2 = view.findViewById(R.id.piechart2);
        tv_tongLoaiThu = view.findViewById(R.id.tv_tongTungLoaiThu);
        barChart = view.findViewById(R.id.barchart);
        date_present = view.findViewById(R.id.date_present);
        date_now = view.findViewById(R.id.date_now);

        setEventClick();
        MainActivity.progressDialogue.show();
        return view;
    }

    public void setEventClick() {
        date_present.setText("01/" + date.getMonth() + "/" + date.getYear());
        date_now.setText(date.getDayMax() + "/" + date.getMonth() + "/" + date.getYear());


        date_present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_present, callback);

            }
        });

        date_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_now, callback1);

            }
        });

    }

    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_present.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            init();
        }
    };
    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback1 = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_now.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            init();
        }
    };

    public void init() {
        long tongChi = 0;
        long tongThu = 0;

        try {
            for (modelkhoanChiThu khoanThu : listKhoanThu) {
                long epdulieukt = Long.parseLong(khoanThu.noiDung);
                tongThu += epdulieukt;
            }

        } catch (Exception e) {
        }
        //Thiet lap du lieu tu sqlite
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //Khai báo xử lý dữ liệu bằng IQ cấp cao của developer
        String TongT1 = "";
        String bienDoi1 = "";
        String mangCatTiepTuc1[];
        //Khai báo xử lý dữ liệu bằng IQ cấp cao của developer

        //ThietLapHienThiPhanTram
        pieChart2.setUsePercentValues(true);
        //ThietLapHienThiPhanTram

        //Xu ly tinh tong tung loai thu
        long mangGan[] = new long[listLoaiThu.size()];
        try {
            for (int i = 0; i < listLoaiThu.size(); i++) {
                long tg = 0;
                for (int j = 0; j < listKhoanThu.size(); j++) {
                    if (ulits.checkDateTo(date_present.getText().toString(), date_now.getText().toString(), listKhoanThu.get(j).getNgay())) {
                        if (listLoaiThu.get(i).tenLoai.contains(listKhoanThu.get(j).tenLoai)) {
                            tg += Long.parseLong(listKhoanThu.get(j).noiDung);
                        }
                    }
                }
                Log.e("tg", tg + "/");
                mangGan[i] = tg;//chuyển qua mảng để sửa dụng vẽ biểu đồ
                if (tg != 0) {
                    TongT1 += "- " + listLoaiThu.get(i).tenLoai + ": " + formatUlits.formatVND(String.valueOf(tg)) + "\n";
                }
            }
            tv_tongLoaiThu.setText(TongT1);
        } catch (Exception e) {

        }

        ///List one
        ArrayList<Entry> yvaluesT = new ArrayList<Entry>();

        try {

            for (int k = 0; k < listLoaiThu.size(); k++) {
                yvaluesT.add(new Entry((mangGan[k] * 100) / tongThu, k));
            }
        } catch (Exception e) {

        }

        PieDataSet dataSet1 = new PieDataSet(yvaluesT, "");
        ///list One

        ///list two
        ArrayList<String> xVals1 = new ArrayList<String>();
        try {
            for (int k = 0; k < listLoaiThu.size(); k++) {
                xVals1.add(listLoaiThu.get(k).tenLoai);
            }
        } catch (Exception e) {

        }

        PieData data1 = new PieData(xVals1, dataSet1);
        ///list two

        data1.setValueFormatter(new PercentFormatter());

        pieChart2.setData(data1);
        //Set color
        dataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
        //dataSet1.setColors(ColorTemplate.JOYFUL_COLORS);
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        //dataSet.setColors(ColorTemplate.LIBERTY_COLORS);
        //dataSet.setColors(ColorTemplate.PASTEL_COLORS);
        //set color

        pieChart2.setDescription("Biểu đồ tròn tổng từng loại thu");

        //text size and text color
        data1.setValueTextSize(13f);
        data1.setValueTextColor(Color.WHITE);
        //text size and text color

        //animation
        pieChart2.animateXY(1500, 1500);
        //animation


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ArrayList<String> labels = new ArrayList<String>();
        try {
            for (int k = 0; k < listLoaiThu.size(); k++) {
                labels.add(listLoaiThu.get(k).tenLoai);
            }
        } catch (Exception e) {

        }

        // create BarEntry for Bar Group 1
        ArrayList<BarEntry> bargroup1 = new ArrayList<>();
        try {

            for (int k = 0; k < listLoaiThu.size(); k++) {
                bargroup1.add(new BarEntry((mangGan[k] * 100) / tongThu, k));
            }
        } catch (Exception e) {

        }

        BarDataSet bardataset = new BarDataSet(bargroup1, "Loại thu");

        BarData data = new BarData(labels, bardataset);
        barChart.setData(data); // set the data and list of labels into chart
        barChart.setDescription("Tỉ lệ % từng loại thu");  // set the description
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        barChart.animateY(5000);
    }

    @Override
    public void onGetSuss(ArrayList<modelLoai> modelLoais) {
        listLoaiThu.clear();
        listLoaiThu = modelLoais;
        Log.e("listLoaiThu", listLoaiThu.size()+"");
        getAPI.getData(sharePre.getIDUsername() + "-thu");
    }

    @Override
    public void fallGet(String e) {
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onGetDataSuss(ArrayList<modelkhoanChiThu> khoanChis) {
        listKhoanThu.clear();
        listKhoanThu = khoanChis;
        Log.e("listLoaiThu", listKhoanThu.size()+"");
        MainActivity.progressDialogue.dismiss();
        init();
    }

    @Override
    public void onfall(String s) {
        MainActivity.progressDialogue.dismiss();
    }
}
