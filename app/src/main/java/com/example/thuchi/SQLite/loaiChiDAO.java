package com.example.thuchi.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.thuchi.Model.modelLoai;

import java.util.ArrayList;

public class loaiChiDAO {
    SQLiteDatabase db;
    Dbhelper dbh;
    public loaiChiDAO(Context c) {
        dbh=new Dbhelper(c);
    }

    public void AddLoaiChi(modelLoai loaiChi){
        db=dbh.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("tenLoai",loaiChi.tenLoai);
        db.insert("loaiChi",null,values);
    }
    public ArrayList<modelLoai> ViewLoaiChi(){
        ArrayList<modelLoai> list_LoaiChi=new ArrayList<modelLoai>();

        db=dbh.getReadableDatabase();
        Cursor c=db.query("loaiChi",null,null,null,null,null,null);
        if (c.moveToFirst()){
            do {
                int _id=c.getInt(0);
                String tenLoaiChi=c.getString(1);
                modelLoai loaiChi=new modelLoai(_id,tenLoaiChi);
                list_LoaiChi.add(loaiChi);
            }while (c.moveToNext());
        }
        return list_LoaiChi;
    }

    public void DeleteLoaiChi(int _id){
        db=dbh.getWritableDatabase();
        db.delete("loaiChi","_id=?",new String[]{_id+""});

    }
    public void UpdateLoaiChi(modelLoai loaiChi){
        db=dbh.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("tenLoai",loaiChi.tenLoai);
        db.update("loaiChi",values,"_id=?",new String[]{loaiChi._id+""});

    }
}
