package com.example.thuchi.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.thuchi.Model.modelLoai;

import java.util.ArrayList;

public class loaiThuDAO {
    Dbhelper dbh;
    SQLiteDatabase db;

    public loaiThuDAO(Context c) {
        dbh = new Dbhelper(c);
    }

    public void AddLoaiThu(modelLoai loaiThu) {
        db = dbh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("tenLoai", loaiThu.tenLoai);
        db.insert("loaiThu", null, values);
    }

    public ArrayList<modelLoai> ViewLoaiThu() {
        ArrayList<modelLoai> listLoaiThu = new ArrayList<modelLoai>();
        db = dbh.getReadableDatabase();
        Cursor c = db.query("loaiThu", null, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                int _id = c.getInt(0);
                String tenLoai = c.getString(1);
                modelLoai thu = new modelLoai(_id, tenLoai);
                listLoaiThu.add(thu);
            } while (c.moveToNext());
        }
        return listLoaiThu;
    }

    public void deleteLoaiThu(int _id) {
        db = dbh.getWritableDatabase();
        db.delete("loaiThu", "_id=?", new String[]{_id+""});
    }

    public void updateLoaiThu(modelLoai loaiThu) {
        db = dbh.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("tenLoai", loaiThu.tenLoai);
        db.update("loaiThu", values, "_id=?", new String[]{loaiThu._id + ""});
    }
}
