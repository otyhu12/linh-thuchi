package com.example.thuchi.MyAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thuchi.R;

import java.util.ArrayList;

public class Adapter_spinnerLoai  extends BaseAdapter {
    Context c;
    ArrayList<String> arrayList = new ArrayList<>();

    public Adapter_spinnerLoai(Context c, ArrayList<String> arrayList) {
        this.c = c;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        convertView = inflater.inflate(R.layout.one_item_sp_loaithu, null);

        TextView tv_item_sp_loaithu = convertView.findViewById(R.id.tv_item_sp_loaithu);
        tv_item_sp_loaithu.setText(arrayList.get(position));

        return convertView;
    }
}
