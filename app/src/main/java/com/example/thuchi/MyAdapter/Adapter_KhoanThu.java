package com.example.thuchi.MyAdapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuchi.API.apiThuChi.DeleteAPI;
import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.apiThuChi.onRepornDatadelete;
import com.example.thuchi.API.apiThuChi.onRepornDataupdate;
import com.example.thuchi.API.apiThuChi.upDateAPI;
import com.example.thuchi.KhoanThuFragment;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.Model.modelkhoanChiThu;
import com.example.thuchi.R;
import com.example.thuchi.SQLite.loaiThuDAO;
import com.example.thuchi.Ulits.formatUlits;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class Adapter_KhoanThu extends RecyclerView.Adapter<Adapter_KhoanThu.MyViewHolder> implements Filterable, onRepornDataupdate, onRepornDatadelete {


    Context context;
    ArrayList<modelkhoanChiThu> listKhoanThu = new ArrayList<modelkhoanChiThu>();
    ArrayList<modelLoai> listLoaiThu = new ArrayList<modelLoai>();
    KhoanThuFragment khoanThuFragment;

    ArrayList<modelkhoanChiThu> listSearch;
    public SharePre sharePre;
    public upDateAPI upDateAPI;
    public DeleteAPI deleteAPI;


    public Adapter_KhoanThu(Context context, ArrayList<modelkhoanChiThu> listKhoanThu, ArrayList<modelLoai> listLoaiThu, KhoanThuFragment khoanThuFragment) {
        this.context = context;
        this.listKhoanThu = listKhoanThu;
        this.listLoaiThu = listLoaiThu;
        this.khoanThuFragment = khoanThuFragment;
        listSearch = new ArrayList<>(listKhoanThu);
        sharePre = new SharePre(context);
        upDateAPI = new upDateAPI(this);
        deleteAPI = new DeleteAPI(this);
    }

    String date;
    Spinner sp_EditLoaiThu;
    loaiThuDAO loaiThuDAO;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_item_for_khoan_thu, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        modelkhoanChiThu khoanThu = listKhoanThu.get(position);

        holder.tv_tenKhoanThu.setText(khoanThu.tenKhoan);
        holder.tv_ngayThu.setText(khoanThu.ngay);
        holder.tv_nguoitao.setText(khoanThu.nguoiNhap);
        holder.tv_SoTienThu.setText(formatUlits.formatVND(khoanThu.noiDung));
        holder.tv_LoaiThu.setText(khoanThu.tenLoai);

        //kiêm tra xem phải do chính chủ nhập thì mới có thể thay đổi được
        if (khoanThu.getIdnguoinhap().contains(apiManager.getUser().getId()) && apiManager.getUser().getId() != "" || khoanThu.getIdnguoinhap() == "") {
            holder.iv_editKhoanThu.setVisibility(View.VISIBLE);
            holder.iv_DeleteKhoanThu.setVisibility(View.VISIBLE);
        } else {
            holder.iv_editKhoanThu.setVisibility(View.GONE);
            holder.iv_DeleteKhoanThu.setVisibility(View.GONE);
        }
        //sự kiện khi ấn xóa button
        holder.iv_DeleteKhoanThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelkhoanChiThu khoanThu1 = listKhoanThu.get(position);
                deleteAPI.delete(context, sharePre.getIDUsername() + "-thu", khoanThu1.get_id());
            }
        });
        //sự kiện khi click vào buton sửa lại thông tin
        holder.iv_editKhoanThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(khoanThuFragment.getContext());
                LayoutInflater inf = khoanThuFragment.getActivity().getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_for_edit_khoan_thu, null);
                ///Lay du lieu va setText
                final modelkhoanChiThu khoanThu = listKhoanThu.get(position);
                final EditText et_EditKhoanThu = view1.findViewById(R.id.et_EditKhoanThu);
                final EditText et_EditNoidungKhoanThu = view1.findViewById(R.id.et_EditNoidungKhoanThu);


                //spinner
                sp_EditLoaiThu = view1.findViewById(R.id.sp_EditLoaiThu);
                loaiThuDAO = new loaiThuDAO(view1.getContext());
                CapNhatGiaoDienSpinnerKhoanThu();
                //Spinner

                //date
                CalendarView EditcalendarView = view1.findViewById(R.id.EditcalendarView);

                EditcalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                        date = dayOfMonth + "/" + (month + 1) + "/" + year;
                    }
                });

                //set date view in calendar
                EditcalendarView.setSelected(true);
                try {
                    String getDate = khoanThu.ngay;
                    String MangDate[] = getDate.split("/");
                    int day = Integer.parseInt(MangDate[0]);
                    int month = Integer.parseInt(MangDate[1]);
                    int year = Integer.parseInt(MangDate[2]);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, (month - 1));
                    calendar.set(Calendar.DAY_OF_MONTH, day);
                    date = day + "/" + (month - 1) + "/" + year;//lấy ra thời gian mặc định
                    long milliTime = calendar.getTimeInMillis();
                    EditcalendarView.setDate(milliTime, true, true);
                } catch (Exception e) {

                }
                //set date view in calendar


                //Spinner
                int IndexSpinner = 0;
                for (int i = 0; i <= listLoaiThu.size() - 1; i++) {
                    if (khoanThu._idLoai == listLoaiThu.get(i)._id) {
                        IndexSpinner = i;
                    }
                }
                sp_EditLoaiThu.setSelection(IndexSpinner);
                //Spinner

                et_EditKhoanThu.setText(khoanThu.tenKhoan);
                et_EditNoidungKhoanThu.setText(khoanThu.noiDung);

                ///Lay du lieu va setText

                alertDialog.setView(view1);

                alertDialog.setNegativeButton("Cập nhật", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tenKhoanThu = et_EditKhoanThu.getText().toString();
                        String noiDung = et_EditNoidungKhoanThu.getText().toString();
                        //String
                        int _idLoaiThu = 0;
                        modelLoai loaiThu = new modelLoai();
                        try {
                            int index = sp_EditLoaiThu.getSelectedItemPosition();
                            loaiThu = listLoaiThu.get(index);
                            _idLoaiThu = loaiThu._id;
                        } catch (Exception e) {

                        }
                        //sp
                        String getDate = date;
                        try {
                            if (getDate == null) {
                                getDate = khoanThu.ngay;
                            }

                        } catch (Exception e) {

                        }
                        //date


                        //SQLite
                        modelkhoanChiThu khoanThu = listKhoanThu.get(position);
                        modelkhoanChiThu khoanThu1 = new modelkhoanChiThu(khoanThu._id, tenKhoanThu, noiDung, getDate, khoanThu.idnguoinhap, khoanThu.nguoiNhap, _idLoaiThu, loaiThu.tenLoai);


                        Map<String, Object> map = new HashMap<>();
                        map.put("_id", khoanThu1._id);
                        map.put("tenKhoan", khoanThu1.tenKhoan);
                        map.put("noiDung", khoanThu1.noiDung);
                        map.put("ngay", khoanThu1.ngay);
                        map.put("idnguoinhap", khoanThu1.idnguoinhap);
                        map.put("nguoiNhap", khoanThu1.nguoiNhap);
                        map.put("_idLoai", khoanThu1._idLoai);
                        map.put("tenLoai", khoanThu1.tenLoai);
                        upDateAPI.Update(context, sharePre.getIDUsername() + "-thu", khoanThu.get_id(), map);

                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(khoanThuFragment.getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });
                alertDialog.show();
                //dialog
            }
        });


    }

    @Override
    public int getItemCount() {
        return listKhoanThu.size();
    }

    @Override
    public Filter getFilter() {
        return filterList;
    }

    //........search
    private Filter filterList = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<modelkhoanChiThu> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listSearch);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (modelkhoanChiThu item : listSearch) {
                    if (item.tenKhoan.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listKhoanThu.clear();
            listKhoanThu.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    @Override
    public void ondeleteDataSuss(JSONObject object) {
        khoanThuFragment.getKhoanThu();
        Toasty.success(khoanThuFragment.getContext(), "Thành công!", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onupdateDataSuss(JSONObject object) {
        khoanThuFragment.getKhoanThu();
        Toasty.success(khoanThuFragment.getContext(), "Thành công!", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onfall(String s) {
        Toasty.success(khoanThuFragment.getContext(), "Thất bại!", Toast.LENGTH_SHORT, true).show();
    }

    //Class nay có thể để bên ngoài
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_tenKhoanThu, tv_SoTienThu, tv_LoaiThu, tv_ngayThu, tv_nguoitao;
        public ImageView iv_editKhoanThu, iv_DeleteKhoanThu;

        public MyViewHolder(View view) {
            super(view);
            this.tv_tenKhoanThu = view.findViewById(R.id.tv_tenKhoanThu);
            this.tv_SoTienThu = view.findViewById(R.id.tv_SoTienThu);
            this.tv_LoaiThu = view.findViewById(R.id.tv_LoaiThu);
            this.tv_ngayThu = view.findViewById(R.id.tv_ngayThu);
            tv_nguoitao = view.findViewById(R.id.tv_nguoitao);
            this.iv_DeleteKhoanThu = view.findViewById(R.id.iv_DeleteKhoanThu);
            this.iv_editKhoanThu = view.findViewById(R.id.iv_editKhoanThu);

        }
    }

    public void CapNhatGiaoDienSpinnerKhoanThu() {
        listLoaiThu = loaiThuDAO.ViewLoaiThu();
        Adapter_SpinnerLoaiThu spinnerLoaiThu = new Adapter_SpinnerLoaiThu(khoanThuFragment.getContext(), listLoaiThu);
        sp_EditLoaiThu.setAdapter(spinnerLoaiThu);
    }

}
