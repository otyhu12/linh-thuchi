package com.example.thuchi.MyAdapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiDinhMuc.apiDinhMucManager;
import com.example.thuchi.API.apiDinhMuc.onDeleteDinhMuc;
import com.example.thuchi.API.apiDinhMuc.onUpdateDinhMuc;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.apiThuChi.DeleteAPI;
import com.example.thuchi.API.apiThuChi.getSumThuChi;
import com.example.thuchi.API.apiThuChi.onSumReporn;
import com.example.thuchi.MainActivity;
import com.example.thuchi.Model.modelDinhMuc;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.R;
import com.example.thuchi.Ulits.formatUlits;
import com.example.thuchi.dinhmucFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Adapter_DinhMuc extends RecyclerView.Adapter<Adapter_DinhMuc.MyViewHolder> implements Filterable, onSumReporn, onUpdateDinhMuc, onDeleteDinhMuc {


    ArrayList<modelDinhMuc> listDinhmuc = new ArrayList<modelDinhMuc>();
    ArrayList<modelLoai> listLoaiChi = new ArrayList<modelLoai>();
    ArrayList<modelDinhMuc> listSearch;
    public Context context;
    dinhmucFragment khoanChiFragment;
    public SharePre sharePre;
    public getSumThuChi getSumThuChi;
    public apiDinhMucManager mucManager;
    public onUpdateDinhMuc onUpdateDinhMuc;
    public ArrayList<String> arrDate;

    String date;
    Spinner sp_EditLoaiChi;
    com.example.thuchi.SQLite.loaiChiDAO loaiChiDAO;
    public com.example.thuchi.API.apiThuChi.upDateAPI upDateAPI;
    public DeleteAPI deleteAPI;

    public Adapter_DinhMuc(ArrayList<modelDinhMuc> listDinhmuc, ArrayList<String> arrDate, Context context, dinhmucFragment dinhmucFragment) {
        this.listDinhmuc = listDinhmuc;
        this.listLoaiChi = listLoaiChi;
        this.context = context;
        this.khoanChiFragment = dinhmucFragment;
        listSearch = new ArrayList<>(listDinhmuc);
        sharePre = new SharePre(context);
        mucManager = new apiDinhMucManager();
        mucManager.onDeleteDinhMuc = this;
        mucManager.onUpdateDinhMuc = this;
        this.arrDate = arrDate;

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_dinhmuc, viewGroup, false);
        return new Adapter_DinhMuc.MyViewHolder(v);
    }
    @Override
    public void onBindViewHolder(Adapter_DinhMuc.MyViewHolder holder, final int position) {
        final modelDinhMuc dinhMuc = listDinhmuc.get(position);

        holder.tv_tenDinhMuc.setText(dinhMuc.getTen());
        holder.tv_loai.setText(getLoai(dinhMuc.getLoai()));
        holder.tv_number.setText(formatUlits.formatVND(dinhMuc.getNumber_c()+"")+"/"+formatUlits.formatVND(dinhMuc.getNumber()+""));
        holder.SeekBar_text.setMax(dinhMuc.number);
        holder.SeekBar_text.setProgress(dinhMuc.number_c);


        //kiêm tra xem phải do chính chủ nhập thì mới có thể thay đổi được
        if (dinhMuc.getId_NguoiTao().contains(apiManager.getUser().getId()) && apiManager.getUser().getId() != "" || dinhMuc.getId_NguoiTao() == "") {
            holder.iv_DeleteDinhMuc.setVisibility(View.VISIBLE);
            holder.iv_editDinhMuc.setVisibility(View.VISIBLE);
        } else {
            holder.iv_DeleteDinhMuc.setVisibility(View.GONE);
            holder.iv_editDinhMuc.setVisibility(View.GONE);
        }
        holder.iv_DeleteDinhMuc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelDinhMuc dinhMuc = listDinhmuc.get(position);
                mucManager.delete(sharePre.getIDUsername()+"-dinhmuc",dinhMuc.getId_firebase());
                MainActivity.progressDialogue.show();
            }
        });


        holder.iv_editDinhMuc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final modelDinhMuc dinhMuc = listDinhmuc.get(position);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(khoanChiFragment.getContext());
                LayoutInflater inf = khoanChiFragment.getActivity().getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_dinhmuc, null);

                final TextView et_tenDinhMuc = view1.findViewById(R.id.et_tenDinhMuc);
                final TextView edt_sotien = view1.findViewById(R.id.et_sotien);
                final Spinner spinner = view1.findViewById(R.id.sp_loai);

                Adapter_spinnerLoai spinnerLoaiChi = new Adapter_spinnerLoai(context, arrDate);
                spinner.setAdapter(spinnerLoaiChi);
                //sét dữ liệu đã dùng
                et_tenDinhMuc.setText(dinhMuc.getTen());
                edt_sotien.setText(dinhMuc.getNumber()+"");
                spinner.setSelection(dinhMuc.loai);
                alertDialog.setView(view1);

                final int[] id_loai = {dinhMuc.loai};

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        id_loai[0] = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                alertDialog.setNegativeButton("Cập nhật", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        modelDinhMuc modelDinhMuc1 = new modelDinhMuc(et_tenDinhMuc.getText().toString(),Integer.parseInt(edt_sotien.getText().toString()), 0,id_loai[0], apiManager.getUser().id);
                        Map<String, Object> map = new HashMap<>();
                        map.put("id_firebase",modelDinhMuc1.id_firebase);
                        map.put("ten",modelDinhMuc1.ten);
                        map.put("loai",modelDinhMuc1.loai);
                        map.put("number",modelDinhMuc1.number);
                        map.put("number_c",modelDinhMuc1.number_c);
                        map.put("id_NguoiTao",modelDinhMuc1.id_NguoiTao);
                        mucManager.Update(sharePre.getIDUsername()+"-dinhmuc",dinhMuc.getId_firebase() , map);
                        MainActivity.progressDialogue.show();
                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
            }
        });

                alertDialog.show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return listDinhmuc.size();
    }

    //..search
    @Override
    public Filter getFilter() {
        return filterList;
    }
    //........search
    private Filter filterList = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<modelDinhMuc> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listSearch);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (modelDinhMuc item : listSearch) {
                    if (item.getTen().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listDinhmuc.clear();
            listDinhmuc.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public String getLoai(int i)
    {
        switch (i){
            case 0:
                return "Ngày";
            case 1:
                return "Tuần";
            case 2:
                return "Tháng";
            case 3:
                return "Năm";
            case 4:
                return "Ngày";
        }
        return "";
    }


    @Override
    public void SumSuss(int value) {
        khoanChiFragment.getDinhmuc();
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void fall(String e) {
        khoanChiFragment.getDinhmuc();
        khoanChiFragment.getDinhmuc();
    }

    @Override
    public void onDeleteDinhMuc(String data) {
        khoanChiFragment.getDinhmuc();
    }

    @Override
    public void onUpdateDinhMuc(String data) {
        khoanChiFragment.getDinhmuc();
    }

    @Override
    public void fallDinhMuc(String e) {
        khoanChiFragment.getDinhmuc();
        MainActivity.progressDialogue.dismiss();
    }
    //.......search
    //Class nay có thể để bên ngoài
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_tenDinhMuc, tv_loai, tv_number;
        public ImageView iv_editDinhMuc, iv_DeleteDinhMuc;
        public SeekBar SeekBar_text;

        public MyViewHolder(View view) {
            super(view);
            this.tv_tenDinhMuc = view.findViewById(R.id.tv_tenDinhMuc);
            this.tv_loai = view.findViewById(R.id.tv_loai);
            this.tv_number = view.findViewById(R.id.tv_number);
            this.SeekBar_text = view.findViewById(R.id.SeekBar_text);
            this.iv_DeleteDinhMuc = view.findViewById(R.id.iv_DeleteDinhMuc);
            this.iv_editDinhMuc = view.findViewById(R.id.iv_editDinhMuc);

        }
    }

    public void CapNhatGiaoDienKhoanSpinnerKhoanChi() {
        listLoaiChi = loaiChiDAO.ViewLoaiChi();
        Adapter_spinnerLoaiChi spinnerLoaiChi = new Adapter_spinnerLoaiChi(khoanChiFragment.getContext(), listLoaiChi);
        sp_EditLoaiChi.setAdapter(spinnerLoaiChi);
    }

}

