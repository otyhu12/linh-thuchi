package com.example.thuchi.MyAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thuchi.Model.modelDinhMuc;
import com.example.thuchi.R;

import java.util.ArrayList;

public class adapterDinhMuc extends BaseAdapter {
    Context c;
    ArrayList<modelDinhMuc> listLoaiChi = new ArrayList<modelDinhMuc>();

    public adapterDinhMuc(Context c, ArrayList<modelDinhMuc> listLoaiChi) {
        this.c = c;
        this.listLoaiChi = listLoaiChi;
    }

    @Override
    public int getCount() {
        return listLoaiChi.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) c).getLayoutInflater();
        convertView = inflater.inflate(R.layout.one_item_sp_loaichi, null);

        TextView tv_item_sp_loaichi = convertView.findViewById(R.id.tv_item_sp_loaichi);

        modelDinhMuc loaiChi = listLoaiChi.get(position);

        tv_item_sp_loaichi.setText(loaiChi.getLoai());


        return convertView;
    }
}