package com.example.thuchi.MyAdapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onDeleteLoai;
import com.example.thuchi.API.apiLoai.onUpdateLoai;
import com.example.thuchi.LoaiThuFragment;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

//import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;

public class AdapterLoaiThu extends RecyclerView.Adapter<AdapterLoaiThu.MyViewHolder> implements Filterable, onDeleteLoai, onUpdateLoai {

    ArrayList<modelLoai> listLoaiThu = new ArrayList<modelLoai>();
    ArrayList<modelLoai> listsearch;//search

    public Context context;
    LoaiThuFragment loaiThuFragment;
    public SharePre sharePre;
    public LoaiApi loaiApi;


    public AdapterLoaiThu(ArrayList<modelLoai> listLoaiThu, Context context, LoaiThuFragment loaiThuFragment) {
        this.listLoaiThu = listLoaiThu;
        this.context = context;
        this.loaiThuFragment = loaiThuFragment;
        listsearch = new ArrayList<>(listLoaiThu);//search
        sharePre = new SharePre(context);
        loaiApi = new LoaiApi();
        loaiApi.onDeleteLoai = this;
        loaiApi.updateLoai = this;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_item_for_loai_thu, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final modelLoai loaiThu = listLoaiThu.get(position);
        holder.tv_ED.setText(loaiThu.tenLoai);
        holder.editLoaiThu.setImageResource(R.drawable.edit);
        holder.deleteLoaiThu.setImageResource(R.drawable.delete);

        if (sharePre.getCheckPemission()) {
            holder.deleteLoaiThu.setVisibility(View.GONE);
            holder.editLoaiThu.setVisibility(View.GONE);
        }
        holder.deleteLoaiThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelLoai loaiThu = listLoaiThu.get(position);
                loaiApi.delete(sharePre.getIDUsername(), loaiThu.id_firebase);
            }
        });


        holder.editLoaiThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(loaiThuFragment.getContext());
                LayoutInflater inf = loaiThuFragment.getActivity().getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_edit_loai_thu, null);

                ///Lay du lieu va setText
                modelLoai loaiThu = listLoaiThu.get(position);
                final EditText et_EditLoaiThu = view1.findViewById(R.id.et_EditLoaiThu);
                et_EditLoaiThu.setText(loaiThu.tenLoai);
                ///Lay du lieu va setText

                alertDialog.setView(view1);

                alertDialog.setNegativeButton("Cập nhật", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tenLoaiThu = et_EditLoaiThu.getText().toString();

                        //SQLite
                        modelLoai loaiThu = listLoaiThu.get(position);
                        loaiThu.setTenLoai(tenLoaiThu);
                        Gson gson = new Gson();
                        Map<String, Object> map = new HashMap<>();
                        map.put("object", gson.toJson(loaiThu));
                        loaiApi.Update(sharePre.getIDUsername(), loaiThu.id_firebase, map);
                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(loaiThuFragment.getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });
                alertDialog.show();
                //dialog
            }
        });


    }

    @Override
    public int getItemCount() {
        return listLoaiThu.size();
    }

    @Override
    public Filter getFilter() {
        return filterList;
    }

    //........search
    private Filter filterList = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<modelLoai> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listsearch);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (modelLoai item : listsearch) {
                    if (item.tenLoai.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listLoaiThu.clear();
            listLoaiThu.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public void onDeleteSuss(ArrayList<modelLoai> modelLoais) {
        loaiThuFragment.getThu();
    }

    @Override
    public void falldelete(String e) {
        loaiThuFragment.getThu();
    }

    @Override
    public void onUpdateSuss(ArrayList<modelLoai> modelLoais) {
        loaiThuFragment.getThu();
    }

    @Override
    public void fallUpdate(String e) {
        loaiThuFragment.getThu();
    }
    //.......search


    //Class nay có thể để bên ngoài
    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_ED;
        public ImageView editLoaiThu, deleteLoaiThu;

        public MyViewHolder(View view) {
            super(view);
            this.tv_ED = view.findViewById(R.id.tv_ED);
            this.deleteLoaiThu = view.findViewById(R.id.iv_DeleteLoaiThu);
            this.editLoaiThu = view.findViewById(R.id.iv_editLoaiThu);
        }
    }

}


