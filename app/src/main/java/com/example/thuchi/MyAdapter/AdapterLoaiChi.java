package com.example.thuchi.MyAdapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onDeleteLoai;
import com.example.thuchi.API.apiLoai.onUpdateLoai;
import com.example.thuchi.LoaiChiFragment;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

//import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;

public class AdapterLoaiChi extends RecyclerView.Adapter<AdapterLoaiChi.MyViewHolder> implements Filterable, onDeleteLoai, onUpdateLoai {
    ArrayList<modelLoai> listLoaiChi = new ArrayList<modelLoai>();
    ArrayList<modelLoai> listsearch1;//search

    Context context;
    LoaiChiFragment loaiChiFragment;
    public SharePre sharePre;
    public LoaiApi loaiApi;

    public AdapterLoaiChi(ArrayList<modelLoai> listLoaiChi, Context context, LoaiChiFragment loaiChiFragment) {
        this.listLoaiChi = listLoaiChi;
        this.context = context;
        this.loaiChiFragment = loaiChiFragment;
        listsearch1 = new ArrayList<>(listLoaiChi);
        sharePre = new SharePre(context);
        loaiApi = new LoaiApi();
        loaiApi.onDeleteLoai = this;
        loaiApi.updateLoai = this;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_item_for_loai_chi, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        modelLoai loaiChi = listLoaiChi.get(position);
        holder.tv_EDC.setText(loaiChi.tenLoai);
        holder.iv_EditLoaiChi.setImageResource(R.drawable.edit);
        holder.iv_DeleteLoaiChi.setImageResource(R.drawable.delete);

        if (sharePre.getCheckPemission()) {
            holder.iv_DeleteLoaiChi.setVisibility(View.GONE);
            holder.iv_EditLoaiChi.setVisibility(View.GONE);
        }
        holder.iv_DeleteLoaiChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelLoai loaiChi1 = listLoaiChi.get(position);
                loaiApi.delete(sharePre.getIDUsername(), loaiChi1.id_firebase);

            }
        });
        holder.iv_EditLoaiChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //.......dialog
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(loaiChiFragment.getContext());
                LayoutInflater inf = loaiChiFragment.getActivity().getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_edit_loai_chi, null);

                ///Lay du lieu va setText
                modelLoai loaiChi = listLoaiChi.get(position);
                final EditText et_EditLoaiChi = view1.findViewById(R.id.et_EditLoaiChi);
                et_EditLoaiChi.setText(loaiChi.tenLoai);
                ///Lay du lieu va setText

                alertDialog.setView(view1);

                alertDialog.setNegativeButton("Cập nhật", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tenLoaiThu = et_EditLoaiChi.getText().toString();

                        //SQLite
                        modelLoai loaiChi = listLoaiChi.get(position);
                        loaiChi.setTenLoai(tenLoaiThu);
                        //SQLite
                        Gson gson = new Gson();
                        Map<String, Object> map = new HashMap<>();
                        map.put("object", gson.toJson(loaiChi));
                        loaiApi.Update(sharePre.getIDUsername(), loaiChi.id_firebase, map);

                        //Toasty.success(loaiChiFragment.getContext(), "Đã cập nhật!", Toast.LENGTH_SHORT, true).show();

                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(loaiChiFragment.getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });
                alertDialog.show();

                //.....dialog
            }
        });


    }

    @Override
    public int getItemCount() {
        return listLoaiChi.size();
    }

    @Override
    public Filter getFilter() {
        return filterList;
    }

    //.........search
    private Filter filterList = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<modelLoai> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listsearch1);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (modelLoai item : listsearch1) {
                    if (item.tenLoai.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listLoaiChi.clear();
            listLoaiChi.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public void onDeleteSuss(ArrayList<modelLoai> modelLoais) {
        loaiChiFragment.getLoat();
    }

    @Override
    public void falldelete(String e) {
        loaiChiFragment.getLoat();
    }

    @Override
    public void onUpdateSuss(ArrayList<modelLoai> modelLoais) {
        loaiChiFragment.getLoat();
    }

    @Override
    public void fallUpdate(String e) {
        loaiChiFragment.getLoat();
    }
    //.......search


    //.......Class nay có thể để bên ngoài
    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_EDC;
        public ImageView iv_EditLoaiChi, iv_DeleteLoaiChi;

        public MyViewHolder(View view) {
            super(view);
            this.tv_EDC = view.findViewById(R.id.tv_EDC);
            this.iv_DeleteLoaiChi = view.findViewById(R.id.iv_DeleteLoaiChi);
            this.iv_EditLoaiChi = view.findViewById(R.id.iv_editLoaiChi);
        }
    }
    //.......Class could outside.
}


