package com.example.thuchi.MyAdapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuchi.API.apiThuChi.DeleteAPI;
import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.apiThuChi.onRepornDatadelete;
import com.example.thuchi.API.apiThuChi.onRepornDataupdate;
import com.example.thuchi.API.apiThuChi.upDateAPI;
import com.example.thuchi.KhoanChiFragment;
import com.example.thuchi.MainActivity;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.Model.modelkhoanChiThu;
import com.example.thuchi.R;
import com.example.thuchi.SQLite.loaiChiDAO;
import com.example.thuchi.Ulits.formatUlits;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class Adapter_KhoanChi extends RecyclerView.Adapter<Adapter_KhoanChi.MyViewHolder> implements Filterable, onRepornDataupdate, onRepornDatadelete {


    ArrayList<modelkhoanChiThu> listKhoanChi = new ArrayList<modelkhoanChiThu>();
    ArrayList<modelLoai> listLoaiChi = new ArrayList<modelLoai>();
    ArrayList<modelkhoanChiThu> listSearch;
    public Context context;
    KhoanChiFragment khoanChiFragment;
    public SharePre sharePre;


    String date;
    Spinner sp_EditLoaiChi;
    loaiChiDAO loaiChiDAO;
    public com.example.thuchi.API.apiThuChi.upDateAPI upDateAPI;
    public DeleteAPI deleteAPI;

    public Adapter_KhoanChi(ArrayList<modelkhoanChiThu> listKhoanChi, ArrayList<modelLoai> listLoaiChi, Context context, KhoanChiFragment khoanChiFragment) {
        this.listKhoanChi = listKhoanChi;
        this.listLoaiChi = listLoaiChi;
        this.context = context;
        this.khoanChiFragment = khoanChiFragment;
        listSearch = new ArrayList<>(listKhoanChi);
        sharePre = new SharePre(context);
        upDateAPI = new upDateAPI(this);
        deleteAPI = new DeleteAPI(this);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_item_for_khoan_chi, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final modelkhoanChiThu khoanChi = listKhoanChi.get(position);

        holder.tv_tenKhoanChi.setText(khoanChi.tenKhoan);
        holder.tv_ngayChi.setText(khoanChi.ngay);
        holder.tv_nguoitao.setText(khoanChi.nguoiNhap);
        holder.tv_SoTienChi.setText(formatUlits.formatVND(khoanChi.noiDung));
        holder.tv_LoaiChi.setText(khoanChi.tenLoai);

        //kiêm tra xem phải do chính chủ nhập thì mới có thể thay đổi được
        if (khoanChi.getIdnguoinhap().contains(apiManager.getUser().getId()) && apiManager.getUser().getId() != "" || khoanChi.getIdnguoinhap() == "") {
            holder.iv_editKhoanChi.setVisibility(View.VISIBLE);
            holder.iv_DeleteKhoanChi.setVisibility(View.VISIBLE);
        } else {
            holder.iv_editKhoanChi.setVisibility(View.GONE);
            holder.iv_DeleteKhoanChi.setVisibility(View.GONE);
        }
        holder.iv_DeleteKhoanChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelkhoanChiThu khoanChi = listKhoanChi.get(position);
                deleteAPI.delete(context, sharePre.getIDUsername() + "-chi", khoanChi.get_id());
            }
        });


        holder.iv_editKhoanChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(khoanChiFragment.getContext());
                LayoutInflater inf = khoanChiFragment.getActivity().getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_for_edit_khoan_chi, null);

                ///Lay du lieu va setText
                final modelkhoanChiThu khoanChi = listKhoanChi.get(position);
                final EditText et_EditKhoanChi = view1.findViewById(R.id.et_EditKhoanChi);
                final EditText et_EditNoidungKhoanChi = view1.findViewById(R.id.et_EditNoidungKhoanChi);

                //spinner
                sp_EditLoaiChi = view1.findViewById(R.id.sp_EditLoaiChi);
                loaiChiDAO = new loaiChiDAO(view1.getContext());
                CapNhatGiaoDienKhoanSpinnerKhoanChi();

                //Spinner

                //date
                CalendarView EditcalendarView = view1.findViewById(R.id.EditcalendarView2);
                EditcalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                        date = dayOfMonth + "/" + (month + 1) + "/" + year;
                    }
                });

                //set date view interface
                try {
                    String getDate = khoanChi.ngay;
                    String MangDate[] = getDate.split("/");
                    int day = Integer.parseInt(MangDate[0]);
                    int month = Integer.parseInt(MangDate[1]);
                    int year = Integer.parseInt(MangDate[2]);
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, (month - 1));
                    calendar.set(Calendar.DAY_OF_MONTH, day);
                    date = day + "/" + (month - 1) + "/" + year;//lấy ra thời gian mặc định
                    long milliTime = calendar.getTimeInMillis();
                    EditcalendarView.setDate(milliTime, true, true);
                } catch (Exception e) {

                }
                //setDate view interface


                //Spinner
                int IndexSpinner = 0;
                for (int i = 0; i <= listLoaiChi.size() - 1; i++) {
                    if (khoanChi._idLoai == listLoaiChi.get(i)._id) {
                        IndexSpinner = i;
                    }
                }
                sp_EditLoaiChi.setSelection(IndexSpinner);
                //Spinner

                et_EditKhoanChi.setText(khoanChi.tenKhoan);
                et_EditNoidungKhoanChi.setText(khoanChi.noiDung);
                ///Lay du lieu va setText

                alertDialog.setView(view1);

                alertDialog.setNegativeButton("Cập nhật", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //String
                        String tenKhoanChi = et_EditKhoanChi.getText().toString();
                        String noiDung = et_EditNoidungKhoanChi.getText().toString();
                        //String

                        //sp
                        int _idLoaiChi = 0;
                        modelLoai loaiChi = new modelLoai();
                        try {

                            int index = sp_EditLoaiChi.getSelectedItemPosition();
                            loaiChi = listLoaiChi.get(index);
                            _idLoaiChi = loaiChi._id;
                        } catch (Exception e) {

                        }
                        //sp

                        //date
                        String getDate = date;
                        try {
                            if (getDate == null) {
                                getDate = khoanChi.ngay;
                            }
                        } catch (Exception e) {

                        }
                        //SQLite
                        modelkhoanChiThu khoanChi = listKhoanChi.get(position);

                        Map<String, Object> map = new HashMap<>();
                        map.put("_id", khoanChi._id);
                        map.put("tenKhoan", tenKhoanChi);
                        map.put("noiDung", noiDung);
                        map.put("ngay", getDate);
                        map.put("idnguoinhap", khoanChi.idnguoinhap);
                        map.put("nguoiNhap", khoanChi.nguoiNhap);
                        map.put("_idLoai", loaiChi._id);
                        map.put("tenLoai", loaiChi.tenLoai);

                        upDateAPI.Update(context, sharePre.getIDUsername() + "-chi", khoanChi.get_id(), map);
                        //SQLite

                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(khoanChiFragment.getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });
                alertDialog.show();

                //dialog
            }
        });


    }

    @Override
    public int getItemCount() {
        return listKhoanChi.size();
    }

    //..search
    @Override
    public Filter getFilter() {
        return filterList;
    }
    //...search

    //........search
    private Filter filterList = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<modelkhoanChiThu> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listSearch);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (modelkhoanChiThu item : listSearch) {
                    if (item.tenKhoan.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listKhoanChi.clear();
            listKhoanChi.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    @Override
    public void ondeleteDataSuss(JSONObject object) {
        khoanChiFragment.getKhoanChi();
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onupdateDataSuss(JSONObject object) {
        khoanChiFragment.getKhoanChi();
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onfall(String s) {

    }
    //.......search


    //Class nay có thể để bên ngoài
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_tenKhoanChi, tv_SoTienChi, tv_LoaiChi, tv_ngayChi, tv_nguoitao;
        public ImageView iv_editKhoanChi, iv_DeleteKhoanChi;

        public MyViewHolder(View view) {
            super(view);
            this.tv_tenKhoanChi = view.findViewById(R.id.tv_tenKhoanChi);
            this.tv_SoTienChi = view.findViewById(R.id.tv_SoTienChi);
            this.tv_LoaiChi = view.findViewById(R.id.tv_LoaiChi);
            this.tv_ngayChi = view.findViewById(R.id.tv_ngaychi);
            this.tv_nguoitao = view.findViewById(R.id.tv_nguoitao);
            this.iv_DeleteKhoanChi = view.findViewById(R.id.iv_DeleteKhoanChi);
            this.iv_editKhoanChi = view.findViewById(R.id.iv_editKhoanChi);

        }
    }

    public void CapNhatGiaoDienKhoanSpinnerKhoanChi() {
        listLoaiChi = loaiChiDAO.ViewLoaiChi();
        Adapter_spinnerLoaiChi spinnerLoaiChi = new Adapter_spinnerLoaiChi(khoanChiFragment.getContext(), listLoaiChi);
        sp_EditLoaiChi.setAdapter(spinnerLoaiChi);
    }

}

