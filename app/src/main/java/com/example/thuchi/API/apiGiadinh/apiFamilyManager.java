package com.example.thuchi.API.apiGiadinh;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.Map;

public class apiFamilyManager {
    public onDeleteFamily onDeleteFamily;
    public onGetFamily onGetFamily;
    public onPostFamily onPostFamily;
    public onUpdateFamily onUpdateFamily;

    public apiFamilyManager() {
    }

    //thêm vào firebase
    public void AddData(String id, Map<String, Object> data)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // Add a new document with a generated ID
        db.collection(id)
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        onPostFamily.onPostFamily("");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onPostFamily.fallFamily("");
                    }
                });

    }
    //lấy ra từ firebase
    public void getData(String id, final boolean check)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //truy vấn dữ liệu
        db.collection(id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                           // ArrayList<modelLoai> khoanChis = new ArrayList<>();
                            QuerySnapshot doc = task.getResult();
                            for (DocumentSnapshot snapshots: doc.getDocuments()) {
//                                modelLoai modelloai = snapshots.toObject(modelLoai.class);
//                                if (modelloai.equals(check)) {
//                                    khoanChis.add(new modelLoai(snapshots.getId(), modelloai.get_id(), modelloai.getTenLoai(), modelloai.loai));
//                                }
                            }
                            onGetFamily.fallFamily("");
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onGetFamily.fallFamily(e.toString());
            }
        });
    }
    //xóa
    public void delete(String id, String idDelete)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id).document(idDelete)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onDeleteFamily.fallFamily(null);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onDeleteFamily.fallFamily(e.toString());
                    }
                });
    }
    public void Update(String id, String idupdate, Map<String, Object> value)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id)
                .document(idupdate)
                .set(value, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onUpdateFamily.onUpdateFamily("");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onUpdateFamily.onUpdateFamily(e.toString());
                    }
                });
    }
}
