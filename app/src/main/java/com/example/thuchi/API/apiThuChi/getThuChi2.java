package com.example.thuchi.API.apiThuChi;

import android.support.annotation.NonNull;

import com.example.thuchi.Model.modelkhoanChiThu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class getThuChi2 {
    public onRepornDataget2 onRepornDataget;
    public getThuChi2(onRepornDataget2 onRepornDataget) {
        this.onRepornDataget = onRepornDataget;

    }

    //lấy ra từ firebase
    public void getData(String id)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //truy vấn dữ liệu
        db.collection(id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<modelkhoanChiThu> khoanChis = new ArrayList<>();
                            QuerySnapshot doc = task.getResult();
                            for (DocumentSnapshot snapshots: doc.getDocuments()) {
                                modelkhoanChiThu modelkhoanChiThu = snapshots.toObject(modelkhoanChiThu.class);
                                khoanChis.add(new modelkhoanChiThu(snapshots.getId(),modelkhoanChiThu.getTenKhoan(),modelkhoanChiThu.getNoiDung(),modelkhoanChiThu.getNgay(),modelkhoanChiThu.getIdnguoinhap(),modelkhoanChiThu.getNguoiNhap(),modelkhoanChiThu.get_idLoai(), modelkhoanChiThu.tenLoai));
                            }
                            onRepornDataget.onGetDataSuss2(khoanChis);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onRepornDataget.onfall(e.toString());
            }
        });
    }
}
