package com.example.thuchi.API.apiThuChi;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import es.dmoral.toasty.Toasty;

public class DeleteAPI {
    public onRepornDatadelete onRepornDatadelete;

    public DeleteAPI(com.example.thuchi.API.apiThuChi.onRepornDatadelete onRepornDatadelete) {
        this.onRepornDatadelete = onRepornDatadelete;
    }

    //xóa
    public void delete(final Context context, String id, String idDelete)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id).document(idDelete)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onRepornDatadelete.ondeleteDataSuss(null);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onRepornDatadelete.onfall("");
                        Toasty.success(context, "Thất bại",Toast.LENGTH_SHORT, true).show();
                    }
                });
    }
}
