package com.example.thuchi.API.apiThuChi;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.Map;

public class upDateAPI {
    public onRepornDataupdate onRepornDataupdate;

    public upDateAPI(com.example.thuchi.API.apiThuChi.onRepornDataupdate onRepornDataupdate) {
        this.onRepornDataupdate = onRepornDataupdate;
    }

    public void Update(final Context context, String id, String idupdate, Map<String, Object> value)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id)
                .document(idupdate)
                .set(value, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onRepornDataupdate.onupdateDataSuss(null);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onRepornDataupdate.onfall(e.toString());
                    }
                });
    }
}
