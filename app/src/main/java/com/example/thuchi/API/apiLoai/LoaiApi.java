package com.example.thuchi.API.apiLoai;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.thuchi.API.request;
import com.example.thuchi.Model.modelLoai;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

public class LoaiApi {
    public onGetLoai onGetLoai;
    public onUpdateLoai updateLoai;
    public onPostLoai onPostLoai;
    public onDeleteLoai onDeleteLoai;

    public LoaiApi() {
    }
    //lấy ra từ firebase
    public void getData(String id, final boolean check)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //truy vấn dữ liệu
        db.collection(id+"-loai")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<modelLoai> khoanChis = new ArrayList<>();
                            QuerySnapshot doc = task.getResult();
                            for (DocumentSnapshot snapshots: doc.getDocuments()) {
                                request request = snapshots.toObject(request.class);
                                if (request.object != "") {
                                    Gson gson = new Gson();
                                    modelLoai modelLoai = gson.fromJson(request.object, com.example.thuchi.Model.modelLoai.class);
                                    if (modelLoai.loai == check) {
                                        Log.e("modelkhoanChiThu", "" + request.object);
                                        khoanChis.add(new modelLoai(snapshots.getId(), modelLoai.get_id(), modelLoai.getTenLoai(), modelLoai.loai));
                                    }
                                }


                            }
                            onGetLoai.onGetSuss(khoanChis);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onGetLoai.fallGet(e.toString());
            }
        });
    }
    //xóa
    public void delete(String id, String idDelete)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id+"-loai").document(idDelete)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onDeleteLoai.onDeleteSuss(null);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onDeleteLoai.falldelete("");
                    }
                });
    }
    //thêm vào firebase
    public void AddData(String id, Map<String, Object> data)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // Add a new document with a generated ID
        db.collection(id+"-loai")
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        onPostLoai.onPostSuss(null);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onPostLoai.fallPost("");
                    }
                });

    }
    public void Update(String id, String idupdate, Map<String, Object> value)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id+"-loai")
                .document(idupdate)
                .set(value, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        updateLoai.onUpdateSuss(null);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        updateLoai.fallUpdate(e.toString());
                    }
                });
    }
}
