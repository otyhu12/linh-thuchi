package com.example.thuchi.API;

import android.os.Bundle;

import com.example.thuchi.API.apiThuChi.onRepornDataUser;
import com.example.thuchi.Model.User;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class facebookAPI {
    public static onRepornDataUser repornData;


    public facebookAPI(onRepornDataUser repornData) {
        this.repornData = repornData;
    }

    public static void RequestDataFacebook(){

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                if(json != null){
                    try {
                         User user = new User(json.getString("id"), json.getString("name"), json.getJSONObject("picture").getJSONObject("data").getString("url"));
                         repornData.onGetDataSuss(user);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

}
