package com.example.thuchi.API.apiDinhMuc;

import androidx.annotation.NonNull;

import com.example.thuchi.Model.modelDinhMuc;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.Map;

public class apiDinhMucManager {
    public onDeleteDinhMuc onDeleteDinhMuc;
    public onGetDinhMuc onGetDinhMuc;
    public onPostDinhMuc onPostDinhMuc;
    public onUpdateDinhMuc onUpdateDinhMuc;

    public apiDinhMucManager() {
    }

    //thêm vào firebase
    public void AddData(String id, Map<String, Object> data)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // Add a new document with a generated ID
        db.collection(id)
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        onPostDinhMuc.onPostDinhMuc("");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onPostDinhMuc.fallDinhMuc("");
                    }
                });

    }
    //lấy ra từ firebase
    public void getData(String id)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //truy vấn dữ liệu
        db.collection(id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                             ArrayList<modelDinhMuc> arr = new ArrayList<>();
                            QuerySnapshot doc = task.getResult();
                            for (DocumentSnapshot snapshots: doc.getDocuments()) {
                                modelDinhMuc modelDinhMuc = snapshots.toObject(modelDinhMuc.class);
                                arr.add(new modelDinhMuc(snapshots.getId(), modelDinhMuc.getTen(), modelDinhMuc.getNumber(), modelDinhMuc.number_c, modelDinhMuc.getLoai(), modelDinhMuc.id_NguoiTao));
                            }
                            onGetDinhMuc.onGetDinhMuc(arr);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onGetDinhMuc.fallDinhMuc(e.toString());
            }
        });
    }
    //xóa
    public void delete(String id, String idDelete)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id).document(idDelete)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onDeleteDinhMuc.fallDinhMuc(null);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onDeleteDinhMuc.fallDinhMuc(e.toString());
                    }
                });
    }
    public void Update(String id, String idupdate, Map<String, Object> value)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(id)
                .document(idupdate)
                .set(value, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        onUpdateDinhMuc.onUpdateDinhMuc("");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        onUpdateDinhMuc.onUpdateDinhMuc(e.toString());
                    }
                });
    }

}
