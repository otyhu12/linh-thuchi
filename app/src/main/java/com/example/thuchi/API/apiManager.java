package com.example.thuchi.API;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.example.thuchi.Model.User;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class apiManager {
    public static User user;
    //tạo mã barcode không có dữ liêu
    public static void generateCode(ImageView imgGenerateBarCode, String code) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = null;
            bitMatrix = multiFormatWriter.encode(code, BarcodeFormat.QR_CODE, 500, 500);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imgGenerateBarCode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public static User getUser() {
        if (user != null){
        return user;}
        else {
            user = new User();
            return user;
        }
    }
}
