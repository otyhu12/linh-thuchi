package com.example.thuchi.API;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePre {
    public static String KEY_ID = "KEY_ID_THUCHI";
    public static String CHECK_PREMISSION_FAMILY = "CHECK_PREMISSION_FAMILY_1";
    public static String USERNAME = "USERNAME_THUCHI";
    public static String ID_USERNAME = "ID_USERNAME";

    private SharedPreferences sharedPreferences;

    public SharePre(Context context) {
        sharedPreferences = context.getSharedPreferences(KEY_ID, Context.MODE_PRIVATE);
    }

    private static SharePre idReferences;

    public static SharePre getInstance(Context context) {
        if (idReferences == null) {
            idReferences = new SharePre(context);
        }
        return idReferences;
    }

    public boolean getCheckPemission() {
        if (sharedPreferences != null) {
            return sharedPreferences.getBoolean(CHECK_PREMISSION_FAMILY, false);
        }
        return false;
    }

    public void putCheckPemission(boolean token) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(CHECK_PREMISSION_FAMILY, token);
        prefsEditor.commit();
    }
    public String getUsername() {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(USERNAME, "");
        }
        return "";
    }

    public void putUsename(String token) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(USERNAME, token);
        prefsEditor.commit();
    }
    public String getIDUsername() {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(ID_USERNAME, "");
        }
        return "";
    }

    public void putIDUsename(String token) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(ID_USERNAME, token);
        prefsEditor.commit();
    }

    public  void clearData(){
        putCheckPemission(false);
        putIDUsename("");
        putUsename("");
    }

}
