package com.example.thuchi;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiDinhMuc.apiDinhMucManager;
import com.example.thuchi.API.apiDinhMuc.onGetDinhMuc;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onGetLoai;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.API.apiThuChi.PostAPI;
import com.example.thuchi.API.apiThuChi.getAPI;
import com.example.thuchi.API.apiThuChi.onRepornDataget;
import com.example.thuchi.API.apiThuChi.onRepornDatapost;
import com.example.thuchi.Date.date;
import com.example.thuchi.Model.modelDinhMuc;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.Model.modelkhoanChiThu;
import com.example.thuchi.MyAdapter.Adapter_KhoanChi;
import com.example.thuchi.MyAdapter.Adapter_spinnerLoaiChi;
import com.example.thuchi.SQLite.loaiChiDAO;
import com.example.thuchi.Ulits.ulits;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class KhoanChiFragment extends Fragment implements onRepornDatapost, onRepornDataget, onGetLoai, onGetDinhMuc {

    FloatingActionButton addKhoanChi;
    public EditText et_KhoanChi, et_NoidungKhoanChi;
    Spinner sp_LoaiChi;
    public AlertDialog.Builder alertDial;
    loaiChiDAO loaiChiDAO;
    CalendarView calendarView2;
    String date1;
    ArrayList<modelLoai> listLoaiChi = new ArrayList<modelLoai>();


    SwipeRefreshLayout swipeRefreshLayout;
    public PostAPI postAPI;
    public getAPI getAPI;


    //RecyclerView
    RecyclerView recyclerViewKhoanChi;
    ArrayList<modelkhoanChiThu> listKhoanChi = new ArrayList<modelkhoanChiThu>();
    //RecyclerView

    //...search
    View view;
    Adapter_KhoanChi adapter_khoanChi;
    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;
    //...search
    public SharePre sharePre;
    public LoaiApi loaiApi;
    public apiDinhMucManager apiDinhMucManager;
    public ArrayList<modelDinhMuc> arrDinhMucs;
    public TextView date_present, date_now;

    public KhoanChiFragment() {
        // Required empty public constructor
    }

    //have to import this void new
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    // have to import this void.

    // Create option menu for toolbar equal lookup, Just use for fragment
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //Have to add Clear with tabLayout
        menu.clear();
        //Have to add Clear with tablayou
        inflater.inflate(R.menu.menu_for_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);

                    //important
                    adapter_khoanChi.getFilter().filter(newText);
                    //important

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
    // Create option menu for toolbar equal lookup, Just use for fragment

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_khoan_chi, null);
        addKhoanChi = view.findViewById(R.id.addKhoanChi);

        date_present = view.findViewById(R.id.date_present);
        date_now = view.findViewById(R.id.date_now);

        sharePre = new SharePre(getContext());
        postAPI = new PostAPI(this);
        getAPI = new getAPI(this);
        loaiApi = new LoaiApi();
        loaiApi.onGetLoai = this;
        apiDinhMucManager = new apiDinhMucManager();
        apiDinhMucManager.onGetDinhMuc = this;
        arrDinhMucs = new ArrayList<>();
        setEventClick();
        getKhoanChi();
        Init();

        recyclerViewKhoanChi = view.findViewById(R.id.recyclerViewKhoanChi);
        //refreshLayout use support v4
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getKhoanChi();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 000);
            }
        });

        //Even click view dialog
        addKhoanChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Init();
                alertDial.setNegativeButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String tenKhoanChi = et_KhoanChi.getText().toString();
                        String noiDung = et_NoidungKhoanChi.getText().toString();

                        //date
                        String getDate = date1;
                        //date

                        //spinner
                        int _idLoaiChi = 0;
                        modelLoai loaiChi = new modelLoai();
                        try {

                            int index = sp_LoaiChi.getSelectedItemPosition();
                            loaiChi = listLoaiChi.get(index);
                            _idLoaiChi = loaiChi._id;

                        } catch (Exception e) {

                        }
                        getDinhMuc();
                        //
                        modelkhoanChiThu khoanChi = new modelkhoanChiThu(tenKhoanChi, noiDung, getDate, apiManager.getUser().id, apiManager.getUser().name, _idLoaiChi, loaiChi.tenLoai);
                        Map<String, Object> map = new HashMap<>();
                        map.put("_id", khoanChi._id);
                        map.put("tenKhoan", khoanChi.tenKhoan);
                        map.put("noiDung", khoanChi.noiDung);
                        map.put("ngay", khoanChi.ngay);
                        map.put("idnguoinhap", khoanChi.idnguoinhap);
                        map.put("nguoiNhap", khoanChi.nguoiNhap);
                        map.put("_idLoai", loaiChi._id);
                        map.put("tenLoai", khoanChi.tenLoai);
                        //map.put("object",khoanChi);

                        postAPI.AddData(getContext(), sharePre.getIDUsername() + "-chi", map);
                    }
                });
                alertDial.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDial.show();

            }
        });

        return view;
    }

    public void setEventClick() {
        date_present.setText("01/" + date.getMonth() + "/" + date.getYear());
        date_now.setText(date.getDayMax() + "/" + date.getMonth() + "/" + date.getYear());


        date_present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_present, callback);

            }
        });

        date_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_now, callback1);

            }
        });

    }

    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_present.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            getKhoanChi();
        }
    };
    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback1 = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_now.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            getKhoanChi();
        }
    };

    public void Init() {
        alertDial = new AlertDialog.Builder(view.getContext());
        LayoutInflater inf = KhoanChiFragment.this.getLayoutInflater();
        View view1 = inf.inflate(R.layout.dialog_khoanchi, null);

        et_KhoanChi = view1.findViewById(R.id.et_KhoanChi);
        et_NoidungKhoanChi = view1.findViewById(R.id.et_NoidungKhoanChi);

        date1 = date.getDayMax() + "/" + (date.getMonth()) + "/" + date.getYear();
        //date
        calendarView2 = view1.findViewById(R.id.calendarView2);
        calendarView2.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                date1 = dayOfMonth + "/" + (month + 1) + "/" + year;
            }
        });

        //date
        // xu ly spinner
        sp_LoaiChi = view1.findViewById(R.id.sp_LoaiChi);
        alertDial.setView(view1);

    }

    public void getDinhmuc() {
        apiDinhMucManager.getData(sharePre.getIDUsername() + "-dinhmuc");
        MainActivity.progressDialogue.show();
    }

    public void getKhoanChi() {
        getAPI.getData(sharePre.getIDUsername() + "-chi");
        loaiApi.getData(sharePre.getIDUsername(), false);
        MainActivity.progressDialogue.show();
    }

    public void CapNhatGiaoDien() {
        adapter_khoanChi = new Adapter_KhoanChi(listKhoanChi, listLoaiChi, getContext(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewKhoanChi.setLayoutManager(linearLayoutManager);
        recyclerViewKhoanChi.setAdapter(adapter_khoanChi);
        adapter_khoanChi.notifyDataSetChanged();
    }

    //cập nhạt lại
    public void CapNhatGiaoDienKhoanSpinnerKhoanChi() {
        // listLoaiChi = loaiChiDAO.ViewLoaiChi();
        Adapter_spinnerLoaiChi spinnerLoaiChi = new Adapter_spinnerLoaiChi(getContext(), listLoaiChi);
        sp_LoaiChi.setAdapter(spinnerLoaiChi);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    //trả về dữ liệu nếu thêm thành công

    //lấy ra dwux liêu
    @Override
    public void onGetDataSuss(ArrayList<modelkhoanChiThu> khoanChis) {

        listKhoanChi.clear();
        for (modelkhoanChiThu modelkhoanChiThu: khoanChis)
        {
            if (ulits.checkDateTo(date_present.getText().toString(), date_now.getText().toString(), modelkhoanChiThu.getNgay())) {

                listKhoanChi.add(modelkhoanChiThu);
            }
        }
        getDinhmuc();
    }

    @Override
    public void onpostDataSuss(JSONObject object) {
        MainActivity.progressDialogue.dismiss();
        getKhoanChi();
    }

    @Override
    public void onfall(String s) {
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onGetSuss(ArrayList<modelLoai> modelLoais) {
        listLoaiChi.clear();
        listLoaiChi = modelLoais;
        CapNhatGiaoDienKhoanSpinnerKhoanChi();
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void fallGet(String e) {
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onGetDinhMuc(ArrayList<modelDinhMuc> data) {
        if (data != null) {
            arrDinhMucs = data;
        }
        CapNhatGiaoDien();
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void fallDinhMuc(String e) {

        CapNhatGiaoDien();
        MainActivity.progressDialogue.dismiss();
    }
    public void getDinhMuc()
    {
        long tong = 0;
        for (modelkhoanChiThu chiThu: listKhoanChi)
        {
            if (ulits.checkDateTo(date_present.getText().toString(), date_now.getText().toString(), chiThu.getNgay()))
            {
                tong += Long.parseLong(chiThu.noiDung);
            }
        }
        for (modelDinhMuc modelDinhMuc: arrDinhMucs)
        {
            if (modelDinhMuc.number < tong)
            {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
                builder.setTitle(modelDinhMuc.ten);
                builder.setMessage("Đã vượt chi tiêu "+getLoai(modelDinhMuc.loai)+"!");
                builder.setCancelable(false);
                builder.setPositiveButton("Đóng", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                android.support.v7.app.AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        }
    }
    public String getLoai(int i)
    {
        switch (i){
            case 0:
                return "Ngày";
            case 1:
                return "Tuần";
            case 2:
                return "Tháng";
            case 3:
                return "Năm";
            case 4:
                return "Ngày";
        }
        return "";
    }
}
