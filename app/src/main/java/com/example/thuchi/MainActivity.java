package com.example.thuchi;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiManager;
import com.example.thuchi.Model.CustomProgressDialogue;
import com.example.thuchi.ViewPager.ScanQR;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;

    ActionBarDrawerToggle toggle;
    public static CustomProgressDialogue progressDialogue;//hiển thị tiến trình load
    public SharePre sharePre;
    MenuItem nav_add;

    @Override
    protected void onResume() {
        super.onResume();
        load();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.shitStuff);
        toolbar = findViewById(R.id.toolBar);
        progressDialogue = new CustomProgressDialogue(this);
        sharePre = new SharePre(this);

        Menu menu = navigationView.getMenu();

        // find MenuItem you want to change
        nav_add = menu.findItem(R.id.add);
        //kiểm tra đã thuốc gia đình hay chưa
        View header = navigationView.getHeaderView(0);
        TextView textUsername = header.findViewById(R.id.tv_name);
        CircleImageView image = header.findViewById(R.id.imageView);
        TextView tv_code = header.findViewById(R.id.tv_code);

        textUsername.setText(apiManager.getUser().getName());
        tv_code.setText(apiManager.getUser().getId());
        try {
            Picasso.with(this).load(apiManager.getUser().getImage()).into(image);
        } catch (Exception e) {

        }
        //sự kiên click vào phần chia sẻ
        tv_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(MainActivity.this);
                LayoutInflater inf = MainActivity.this.getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_image, null);
                ImageView imageView = view1.findViewById(R.id.image_);
                apiManager.generateCode(imageView, apiManager.getUser().id);
                alertDialog.setView(view1);


                alertDialog.setPositiveButton("Đóng", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        //set toolbar thay the cho actionbar
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        //set toolbar thay the cho actionbar

        //set color opacity navigation drawer auto with system.
        getWindow().setStatusBarColor(Color.parseColor("#20111111"));
        //set color opacity navigation drawer auto with system

        //Even when click on item navigationDrawer
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                XuLyChonMenu(item);
                return false;
            }
        });
        //Even when click on item of navigationdrawer

        //set icon auto for navigation drawer
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        //set icon auto for navigation drawer
    }

    //////////////////////////////
    public void XuLyChonMenu(MenuItem item) {
        Fragment fragment = null;
        Class classfragment = null;
        //
        switch (item.getItemId()) {
            case R.id.add:
                if (sharePre.getCheckPemission()) {
                    sharePre.putCheckPemission(false);
                    sharePre.putIDUsename(apiManager.getUser().id);
                    load();
                } else {
                    Intent i1 = new Intent(MainActivity.this, ScanQR.class);
                    startActivity(i1);
                }

                break;
            case R.id.Dinhmuc:
                classfragment = dinhmucFragment.class;
                break;
            case R.id.KThu:
                classfragment = ThuFragment.class;
                break;
            case R.id.KChi:
                classfragment = ChiFragment.class;
                break;
            case R.id.TKe:
                classfragment = ThongKeTabFragment.class;
                break;
            case R.id.GThieu:
                Intent i = new Intent(MainActivity.this, InforActivity.class);
                startActivity(i);
                break;
            case R.id.Thoat:
                DialogExit();
                break;
        }


        try {
            fragment = (Fragment) classfragment.newInstance();

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();

            //Even when click on item for navigation Drawer
            item.setChecked(true);
            setTitle(item.getTitle());
            drawerLayout.closeDrawer(GravityCompat.START);
            //Even when click on item for navigation drawer

        } catch (Exception e) {
        }
    }

    public void load() {
        if (sharePre.getCheckPemission()) {
            nav_add.setTitle("Thoát");
        } else {
            nav_add.setTitle("Thêm");
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, new ChiFragment()).commit();
    }

    //////////////////////////////
    public void DialogExit() {
        //View dialog exit for even click on item exit of navigationDrawer
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inf = MainActivity.this.getLayoutInflater();
        View view = inf.inflate(R.layout.dialog_exit, null);
        alertDialog.setView(view);
        alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                disconnectFromFacebook();
            }
        });
        alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
        //View dialog exit for even click on item exit of navigationDrawer
    }

    public void disconnectFromFacebook() {

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        if (isLoggedIn) {
            LoginManager.getInstance().logOut();
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
        }


    }
}

