package com.example.thuchi;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiThuChi.getAPI;
import com.example.thuchi.API.apiThuChi.getThuChi2;
import com.example.thuchi.API.apiThuChi.onRepornDataget;
import com.example.thuchi.API.apiThuChi.onRepornDataget2;
import com.example.thuchi.Date.date;
import com.example.thuchi.Model.modelkhoanChiThu;
import com.example.thuchi.Ulits.formatUlits;
import com.example.thuchi.Ulits.ulits;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThongKeFragment extends Fragment implements onRepornDataget, onRepornDataget2 {

    PieChart piechart;
    TextView tv_TongThu, tv_TongChi, date_present, date_now;
    ArrayList<modelkhoanChiThu> listKhoanThu;
    ArrayList<modelkhoanChiThu> listKhoanChi;

    public getAPI getAPI;
    public getThuChi2 getThuChi2;
    public SharePre sharePre;


    public ThongKeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_thong_ke, null);
        tv_TongThu = view.findViewById(R.id.tv_TongThu);
        tv_TongChi = view.findViewById(R.id.tv_TongChi);
        date_present = view.findViewById(R.id.date_present);
        date_now = view.findViewById(R.id.date_now);

        getAPI = new getAPI(this);
        getThuChi2 = new getThuChi2(this);
        listKhoanThu = new ArrayList<>();
        listKhoanChi = new ArrayList<>();

        piechart = view.findViewById(R.id.piechart);
        sharePre = new SharePre(getContext());

        getAPI.getData(sharePre.getIDUsername() + "-thu");//lấy ra các khoản thu
        setEventClick();

        return view;
    }

    public void setEventClick() {
        date_present.setText("01/" + date.getMonth() + "/" + date.getYear());
        date_now.setText(date.getDayMax() + "/" + date.getMonth() + "/" + date.getYear());


        date_present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_present, callback);

            }
        });

        date_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulits.showDatePickerDialog(getContext(), date_now, callback1);

            }
        });

    }

    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_present.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            init();
        }
    };
    //sự kiện khi chọn ngày tháng
    DatePickerDialog.OnDateSetListener callback1 = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year,
                              int monthOfYear,
                              int dayOfMonth) {
            date_now.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            init();
        }
    };

    public void init() {
        long tongChi = 0;
        long tongThu = 0;
        try {
            for (modelkhoanChiThu khoanThu : listKhoanThu) {
                if (ulits.checkDateTo(date_present.getText().toString(), date_now.getText().toString(), khoanThu.getNgay())) {
                    long epdulieukt = Long.parseLong(khoanThu.noiDung);
                    tongThu += epdulieukt;
                }
            }

        } catch (Exception e) {
        }
        tv_TongThu.setText("- Tổng thu: " + formatUlits.formatVND(String.valueOf(tongThu)));

        try {
            for (modelkhoanChiThu khoanChi : listKhoanChi) {
                if (ulits.checkDateTo(date_present.getText().toString(), date_now.getText().toString(), khoanChi.getNgay())) {
                    long epdulieukc = Long.parseLong(khoanChi.noiDung);
                    tongChi += epdulieukc;
                }
            }

        } catch (Exception e) {
        }

        long tongThuChi = tongChi + tongThu;
        long fthu = 0;
        long fchi = 0;
        try {
            fthu = ((tongThu * 100) / tongThuChi);
            fchi = (tongChi * 100) / tongThuChi;
        } catch (Exception e) {

        }
        //Thiet lạp hiển thị phần trăm
        piechart.setUsePercentValues(true);
        //Thiet lap hiem thi phan tram

        ///List one
        ArrayList<Entry> yvalues = new ArrayList<Entry>();
        yvalues.add(new Entry(fthu, 0));
        yvalues.add(new Entry(fchi, 1));
        PieDataSet dataSet = new PieDataSet(yvalues, "");
        ///list One

        ///list two
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("Khoản thu");
        xVals.add("Khoản chi");

        PieData data = new PieData(xVals, dataSet);
        ///list two

        data.setValueFormatter(new PercentFormatter());

        piechart.setData(data);


        //Set color
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        //dataSet.setColors(ColorTemplate.LIBERTY_COLORS);
        //dataSet.setColors(ColorTemplate.PASTEL_COLORS);
        //set color

        piechart.setDescription("Biểu đồ tròn tổng thu, chi");

        //text size and text color
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.WHITE);
        //text size and text color

        //hiệu ứng của biểu đồ
        piechart.animateXY(1500, 1500);

        tv_TongChi.setText("- Tổng chi: " + formatUlits.formatVND(String.valueOf(tongChi)));


    }

    @Override
    public void onGetDataSuss(ArrayList<modelkhoanChiThu> khoanChis) {
        listKhoanThu = khoanChis;
        getThuChi2.getData(sharePre.getIDUsername() + "-chi");//lấy ra các khảon chi

    }

    @Override
    public void onGetDataSuss2(ArrayList<modelkhoanChiThu> khoanChis) {
        listKhoanChi = khoanChis;

        init();
    }

    @Override
    public void onfall(String s) {

    }
}
