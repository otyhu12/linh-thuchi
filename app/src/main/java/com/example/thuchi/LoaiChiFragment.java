package com.example.thuchi;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thuchi.API.SharePre;
import com.example.thuchi.API.apiLoai.LoaiApi;
import com.example.thuchi.API.apiLoai.onGetLoai;
import com.example.thuchi.API.apiLoai.onPostLoai;
import com.example.thuchi.Model.modelLoai;
import com.example.thuchi.MyAdapter.AdapterLoaiChi;
import com.example.thuchi.SQLite.loaiChiDAO;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoaiChiFragment extends Fragment implements onGetLoai, onPostLoai {
    FloatingActionButton addLoaiChi;
    EditText et_AddLoaiChi;

    SwipeRefreshLayout swipeRefreshLayout;

    //RecyclerView
    RecyclerView recyclerViewLoaiChi;
    ArrayList<modelLoai> listLoaiChi = new ArrayList<>();
    //RecyclerView

    loaiChiDAO loaiChiDAO;
    modelLoai loaiChi;

    //search
    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;
    View view;
    AdapterLoaiChi adapter;
    //search

    //khai báo biến để thực hiện các dữ liêu liên quan đến api
    public LoaiApi loaiApi;
    public SharePre sharePre;

    public LoaiChiFragment() {
        //Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_loai_chi, null);
        addData();
        capnhatGiaodien();
        return view;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    // Create option menu for toolbar equal lookup, Just use for fragment
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        //Have to had clear to auto add icon
        menu.clear();
        //Have to had clear to auto add icon

        inflater.inflate(R.menu.menu_for_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);

                    //important
                    adapter.getFilter().filter(newText);
                    //important

                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }
    //create option menu for toolbar equal lookup, Just use for fragment

    @SuppressLint("RestrictedApi")
    public void addData() {
        loaiApi = new LoaiApi();
        loaiApi.onGetLoai = this;
        loaiApi.onPostLoai = this;
        sharePre = new SharePre(getContext());
        getLoat();
        //even view dialog add loai chi
        addLoaiChi = view.findViewById(R.id.addLoaiChi);
        //kiểm tra xem có đang ở trong nhóm thân nhân hay không
        if (sharePre.getCheckPemission()) {
            addLoaiChi.setVisibility(View.GONE);
        }
        addLoaiChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
                LayoutInflater inf = LoaiChiFragment.this.getLayoutInflater();
                View view1 = inf.inflate(R.layout.dialog_loaichi, null);
                et_AddLoaiChi = view1.findViewById(R.id.et_AddLoaiChi);

                alertDialog.setView(view1);

                alertDialog.setNegativeButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tenLoaiChi = et_AddLoaiChi.getText().toString();
                        //SQLite
                        loaiChi = new modelLoai(tenLoaiChi, false);
                        //loaiChiDAO = new loaiChiDAO(getContext());
                        //loaiChiDAO.AddLoaiChi(loaiChi);
                        //SQLite
                        Gson gson = new Gson();
                        Map<String, Object> map = new HashMap<>();
                        map.put("object", gson.toJson(loaiChi));
                        loaiApi.AddData(sharePre.getIDUsername(), map);


                    }
                });
                alertDialog.setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toasty.error(getContext(), "Đã hủy", Toast.LENGTH_SHORT, true).show();
                    }
                });

                alertDialog.show();
            }
        });
        //even add dialog add loai chi
    }

    public void getLoat() {
        loaiApi.getData(sharePre.getIDUsername(), false);
        MainActivity.progressDialogue.show();
    }

    public void capnhatGiaodien() {

        recyclerViewLoaiChi = view.findViewById(R.id.recyclerViewLoaiChi);
//        loaiChiDAO = new loaiChiDAO(view.getContext());
//
//        //View Loai Thu
//        listLoaiChi = loaiChiDAO.ViewLoaiChi();
        //View Loai Thu

        recyclerViewLoaiChi.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        adapter = new AdapterLoaiChi(listLoaiChi, getContext(), this);

        recyclerViewLoaiChi.setLayoutManager(linearLayoutManager);
        recyclerViewLoaiChi.setAdapter(adapter);
        MainActivity.progressDialogue.dismiss();
    }

    @Override
    public void onGetSuss(ArrayList<modelLoai> modelLoais) {
        listLoaiChi.clear();
        listLoaiChi = modelLoais;
        capnhatGiaodien();

    }

    @Override
    public void fallGet(String e) {

    }

    @Override
    public void onPostSuss(ArrayList<modelLoai> modelLoais) {
        getLoat();
    }

    @Override
    public void fallPost(String e) {

    }
}
