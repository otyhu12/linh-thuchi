package com.example.thuchi.Date;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class date {

    //lấy ra ngày hiện tại
    public static int getDayOfMonth() {
        final Calendar now = GregorianCalendar.getInstance();
        final int dayNumber = now.get(Calendar.DAY_OF_MONTH);
        return dayNumber;
    }
    //lấy ra tháng hiện tại
    public static int getMonth() {
        final Calendar now = GregorianCalendar.getInstance();
        final int month = now.get(Calendar.MONTH)+1;
        return month;
    }
    //lấy ra năm hiện tại
    public static int getYear() {
        final Calendar now = GregorianCalendar.getInstance();
        final int month = now.get(Calendar.YEAR);
        return month;
    }
    //lấy ra ngày cuối cùng cảu tháng
    public static int getDayMax() {
            Calendar calendar = Calendar.getInstance();
            return calendar.getActualMaximum(Calendar.DATE);
    }


}
